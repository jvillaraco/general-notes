COMMON OPERATIONS IN PYTHON, SQLALCHEMY, NGINX, GIT, KUBERNETES, SYSTEMD AND MANY MORE
========================================================================================
**My personal notes in a not defined order**

- [Ansible](#ansible)
- [bash](#bash)
- [curl](#curl)
- [Debian](#debian)
- [dig](#dig)
- [Docker](#docker)
- [Docker Swarm](#docker-swarm)
- [Docker composer](#docker-composer)
- [find](#find)
- [GIT](#git)
- [Helm](#helm)
- [k8s - Kubernetes](#k8s---kubernetes)
- [Libvirt](#libvirt)
- [mariadb / mysql](#mariadb)
- [Makefile](#makefile)
- [NGINX](#nginx)
- [nmap](#nmap)
- [Systemd services](#systemd-services)
- [ssh](#ssh)
- [postgres](#postgres)
- [python](#python)
- [Redmine](#redmine)
- [vi](#vi)
- [YUM (Redhat)](#yum-redhat)

---

- [COMMON OPERATIONS IN PYTHON, SQLALCHEMY, NGINX, GIT, KUBERNETES, SYSTEMD AND MANY MORE](#common-operations-in-python-sqlalchemy-nginx-git-kubernetes-systemd-and-many-more)
- [Ansible](#ansible)
  - [Execute command in multiple servers](#execute-command-in-multiple-servers)
  - [Playbooks](#playbooks)
  - [playbook with multiple tags](#playbook-with-multiple-tags)
  - [index in a loop](#index-in-a-loop)
  - [Index and conditionals](#index-and-conditionals)
  - [Blocks](#blocks)
  - [Blocks and error management](#blocks-and-error-management)
- [bash](#bash)
  - [5 Uses of Set Commands To Make Your Bash Scripts Secure and Robust](#5-uses-of-set-commands-to-make-your-bash-scripts-secure-and-robust)
  - [bash shellscript best practices - trap handler](#bash-shellscript-best-practices---trap-handler)
  - [bash shellscript best practices - ‘set’ builtins — fail fast](#bash-shellscript-best-practices---set-builtins--fail-fast)
  - [bash command combinations](#bash-command-combinations)
  - [bash calculator](#bash-calculator)
  - [Bash: execute a script only when no other instance of the script is running](#bash-execute-a-script-only-when-no-other-instance-of-the-script-is-running)
  - [Bash Hash Table/Dictionary](#bash-hash-tabledictionary)
  - [bash arrays](#bash-arrays)
  - [Webserver](#webserver)
  - [Show animation while executing a long time task](#show-animation-while-executing-a-long-time-task)
  - [Multiprocessing in bash](#multiprocessing-in-bash)
  - [Colors \& styles in term](#colors--styles-in-term)
  - [Readonly variables (constants)](#readonly-variables-constants)
  - [Standard variables](#standard-variables)
  - [for loop](#for-loop)
  - [sed](#sed)
  - [if](#if)
  - [Memory leak](#memory-leak)
  - [bash pretty printing](#bash-pretty-printing)
  - [Change symblink](#change-symblink)
  - [Count hit per second NGINX](#count-hit-per-second-nginx)
  - [Aliases and functions (.bashrc)](#aliases-and-functions-bashrc)
  - [bash expansion](#bash-expansion)
  - [bash echoing every line](#bash-echoing-every-line)
  - [aliases in .bashrc](#aliases-in-bashrc)
- [curl](#curl)
  - [No progress meter (-s)](#no-progress-meter--s)
  - [Check service health](#check-service-health)
  - [Test telnet functionality](#test-telnet-functionality)
  - ["Host:" header](#host-header)
  - [Follow redirects](#follow-redirects)
  - [Supress output](#supress-output)
- [Debian](#debian)
  - [backports config](#backports-config)
  - [Find packages in all repos](#find-packages-in-all-repos)
  - [install from backports](#install-from-backports)
- [Diagram as a code](#diagram-as-a-code)
- [dig](#dig)
  - [Get GLUE NS Records](#get-glue-ns-records)
- [Docker](#docker)
  - [Running as non-root user](#running-as-non-root-user)
  - [docker rm -f $(docker ps -a -q)](#docker-rm--f-docker-ps--a--q)
  - [docker system prune -f](#docker-system-prune--f)
  - [Create efimer debian docker container daemon](#create-efimer-debian-docker-container-daemon)
- [Docker Swarm](#docker-swarm)
  - [Nodes info](#nodes-info)
  - [Services in a node](#services-in-a-node)
  - [Service info](#service-info)
  - [Forced service reload](#forced-service-reload)
- [Docker composer](#docker-composer)
  - [Start up all services](#start-up-all-services)
  - [Start up all services in dettached mode](#start-up-all-services-in-dettached-mode)
- [find](#find)
  - [Find 50 biggest files](#find-50-biggest-files)
  - [Using find to grep on just a selected files](#using-find-to-grep-on-just-a-selected-files)
  - [Using find with xargs](#using-find-with-xargs)
- [GIT](#git)
  - [git code operations types](#git-code-operations-types)
  - [.gitignore template](#gitignore-template)
  - [git remote show origin](#git-remote-show-origin)
  - [git clone --single-branch --branch  host:/dir.git](#git-clone---single-branch---branch--hostdirgit)
  - [git clone --single-branch --branch HAL6 \<server\>:/servers/servers.git](#git-clone---single-branch---branch-hal6-serverserversserversgit)
  - [git branch](#git-branch)
  - [git branch my2.6.14 v2.6.14](#git-branch-my2614-v2614)
  - [git checkout my2.6.14](#git-checkout-my2614)
  - [git branch --set-upstream-to=origin/\<branca\>](#git-branch---set-upstream-tooriginbranca)
  - [git commit -m "\<text\>"](#git-commit--m-text)
  - [Merge a master de la branca:](#merge-a-master-de-la-branca)
  - [Merge de branca \<branca\> a partir del master:](#merge-de-branca-branca-a-partir-del-master)
  - [git log](#git-log)
  - [git log -p](#git-log--p)
  - [git diff](#git-diff)
  - [git push -f origin \<last\_known\_good\_commit\>:\<branch\_name\>](#git-push--f-origin-last_known_good_commitbranch_name)
  - [git push -f origin db773cff80:\<branca\>](#git-push--f-origin-db773cff80branca)
  - [git push -f](#git-push--f)
  - [git push --set-upstream origin TIN-1986\_backend\_test\_if\_create\_method\_is\_necessary](#git-push---set-upstream-origin-tin-1986_backend_test_if_create_method_is_necessary)
  - [Crear repositori inicial:](#crear-repositori-inicial)
  - [git checkout -- public/bin/control/README.md](#git-checkout----publicbincontrolreadmemd)
  - [Git rebase](#git-rebase)
  - [git pull origin master](#git-pull-origin-master)
  - [git reset --hard](#git-reset---hard)
  - [git reset --hard origin/master](#git-reset---hard-originmaster)
  - [git reset --soft HEAD@{1}](#git-reset---soft-head1)
    - [Prova:](#prova)
  - [git show main:README.md](#git-show-mainreadmemd)
  - [git rev-list --all | xargs git grep -F ‘font-size: 52 px;’](#git-rev-list---all--xargs-git-grep--f-font-size-52-px)
  - [git ssh key](#git-ssh-key)
  - [git config --global](#git-config---global)
  - [git config (local)](#git-config-local)
  - [Get local track of a remorte branch](#get-local-track-of-a-remorte-branch)
  - [Delete local \& remote branch](#delete-local--remote-branch)
  - [synchronize your branch list](#synchronize-your-branch-list)
  - [git stash](#git-stash)
  - [Move uncommitted changes to new/existing branch](#move-uncommitted-changes-to-newexisting-branch)
  - [Rewrite my commit text uploaded by error](#rewrite-my-commit-text-uploaded-by-error)
  - [I have accidentally committed to master](#i-have-accidentally-committed-to-master)
- [GraphViz](#graphviz)
  - [Execució](#execució)
- [Helm](#helm)
  - [helm install --dryrun](#helm-install---dryrun)
  - [help install (sense repo)](#help-install-sense-repo)
  - [helm list - List all deployed charts on my system](#helm-list---list-all-deployed-charts-on-my-system)
  - [helm show](#helm-show)
  - [helm show chart](#helm-show-chart)
    - [helm show values](#helm-show-values)
  - [helm status ](#helm-status-)
  - [helm upgrade](#helm-upgrade)
  - [helm repo](#helm-repo)
    - [heml repo add](#heml-repo-add)
    - [help repo list](#help-repo-list)
  - [help install](#help-install)
  - [helm uninstall - Deinstall](#helm-uninstall---deinstall)
  - [Desplegar Help](#desplegar-help)
  - [Verificar helm](#verificar-helm)
- [journalctl logs](#journalctl-logs)
  - [System boots](#system-boots)
  - [service journal logs](#service-journal-logs)
- [Linux commands](#linux-commands)
  - [lsof](#lsof)
- [Makefile](#makefile)
- [NGINX](#nginx)
- [python](#python)
  - [Python command line](#python-command-line)
  - [Python type hints](#python-type-hints)
  - [Docs](#docs)
  - [Multithreading](#multithreading)
  - [Maximum value of a list](#maximum-value-of-a-list)
  - [print hacks](#print-hacks)
    - [clear a line after print it](#clear-a-line-after-print-it)
  - [Usefull decorators](#usefull-decorators)
    - [@lru\_cache: Speed Up Your Programs by Caching](#lru_cache-speed-up-your-programs-by-caching)
    - [@cached\_property: Cache the Result of a Method as an Attribute](#cached_property-cache-the-result-of-a-method-as-an-attribute)
  - [Comprehesion / one liners](#comprehesion--one-liners)
  - [time functions](#time-functions)
    - [timeit](#timeit)
    - [time](#time)
    - [time as a decorator](#time-as-a-decorator)
  - [Param](#param)
    - [Parametres amb nom i tipus, funció tipeada](#parametres-amb-nom-i-tipus-funció-tipeada)
    - [Passing multiples sets of args](#passing-multiples-sets-of-args)
    - [def print\_genius(\*names):](#def-print_geniusnames)
    - [def top\_genius(\*\*names):](#def-top_geniusnames)
    - [def func(\*args, \*\*kwargs):](#def-funcargs-kwargs)
  - [Literal parameter (Python3.8)](#literal-parameter-python38)
  - [Lists](#lists)
    - [Delete all elements in a list](#delete-all-elements-in-a-list)
    - [Access](#access)
  - [Dict](#dict)
    - [Sort dicts (lambda and itemgetter)](#sort-dicts-lambda-and-itemgetter)
  - [Collections.counter](#collectionscounter)
  - [Permutations](#permutations)
    - [itertools.permutations() generates permutations](#itertoolspermutations-generates-permutations)
    - [for an iterable. Time to brute-force those passwords ;-)](#for-an-iterable-time-to-brute-force-those-passwords--)
  - [f-string](#f-string)
    - [multiline](#multiline)
    - [Repr \& str](#repr--str)
    - [Nested f-strings](#nested-f-strings)
    - [f-string Formating](#f-string-formating)
    - [f-string floats](#f-string-floats)
    - [f-tring date representation](#f-tring-date-representation)
    - [f-string for debuging in python 3.8](#f-string-for-debuging-in-python-38)
  - [objects](#objects)
    - [Redefine ==](#redefine-)
    - [@dataclass decorator \>3.7](#dataclass-decorator-37)
    - [Abstract class](#abstract-class)
    - [Separate Class-Level and Instance-Level Attributes](#separate-class-level-and-instance-level-attributes)
    - [Separate Public, Protected and Private Attributes](#separate-public-protected-and-private-attributes)
    - [Use @property Decorator To Control Attributes Precisely](#use-property-decorator-to-control-attributes-precisely)
    - [Use Static Methods in Classes](#use-static-methods-in-classes)
    - [Separate __new__ and __init__: Two Different Python Constructors](#separate-new-and-init-two-different-python-constructors)
    - [Use __slots__ for Better Attributes Control](#use-slots-for-better-attributes-control)
    - [@classmethod vs @staticmethod vs "plain" methods](#classmethod-vs-staticmethod-vs-plain-methods)
  - [copy Vs copy.copy() Vs copy.deepcopy()](#copy-vs-copycopy-vs-copydeepcopy)
  - [Input](#input)
  - [If](#if-1)
    - [switchcase](#switchcase)
  - [Loops](#loops)
    - [Enumerations](#enumerations)
  - [Maths](#maths)
    - [gcd (mcd)](#gcd-mcd)
    - [lcd (mcm](#lcd-mcm)
  - [Zip](#zip)
    - [range()](#range)
    - [Enumerate()](#enumerate)
    - [matrix](#matrix)
  - [regex](#regex)
    - [patter maching](#patter-maching)
  - [Pandas](#pandas)
  - [ipython3 notebook](#ipython3-notebook)
  - [@classmethod vs @staticmethod vs "plain" methods](#classmethod-vs-staticmethod-vs-plain-methods-1)
  - [Lampda expressions](#lampda-expressions)
  - [Infinity values in Python](#infinity-values-in-python)
  - [Using ‘pprint’ to print stuff nicely](#using-pprint-to-print-stuff-nicely)
  - [Unprinting stuff in Python](#unprinting-stuff-in-python)
  - [Python has a backspace character](#python-has-a-backspace-character)
  - [The walrus operator](#the-walrus-operator)
- [Python-modules](#python-modules)
  - [json.tool](#jsontool)
  - [Logging](#logging)
  - [Loguru: Logs](#loguru-logs)
  - [pycg and pyvis: Automated call grafs](#pycg-and-pyvis-automated-call-grafs)
  - [PymMouse: Automate Mouse Actions](#pymmouse-automate-mouse-actions)
  - [argparse](#argparse)
  - [pyperf: Performance analysis (v \> 3,10)](#pyperf-performance-analysis-v--310)
  - [Faker: Faker is a Python package that generates fake personal data for you](#faker-faker-is-a-python-package-that-generates-fake-personal-data-for-you)
  - [Instant http server](#instant-http-server)
  - [argparse](#argparse-1)
- [Systemd services](#systemd-services)
  - [List services](#list-services)
  - [List enabled services](#list-enabled-services)
- [Systemd timers](#systemd-timers)
  - [List timers](#list-timers)
- [Netbox](#netbox)
  - [Netbox dev dict](#netbox-dev-dict)
- [mariadb](#mariadb)
  - [update](#update)
  - [update date](#update-date)
  - [update datetime adding some time](#update-datetime-adding-some-time)
  - [update datetime directly](#update-datetime-directly)
  - [insert](#insert)
    - [insert now()](#insert-now)
    - [insert utc time](#insert-utc-time)
    - [insert with columns](#insert-with-columns)
    - [insert a dict in a text field](#insert-a-dict-in-a-text-field)
  - [delete](#delete)
  - [show grants](#show-grants)
  - [Show servername](#show-servername)
  - [Show variables](#show-variables)
- [YUM (Redhat)](#yum-redhat)
  - [List packages](#list-packages)
    - [yum list installed http\*](#yum-list-installed-http)
    - [yum list-security --security](#yum-list-security---security)
- [VScode - VSCodium](#vscode---vscodium)
  - [Find \& Replace](#find--replace)
    - [Regex for newline](#regex-for-newline)
- [Redmine](#redmine)
  - [External links](#external-links)
  - [Textile formating](#textile-formating)
  - [Wiki formating link](#wiki-formating-link)
  - [Wiki formating text color](#wiki-formating-text-color)
  - [Redmine and git](#redmine-and-git)
  - [Redmine repos](#redmine-repos)
  - [Link to a commit](#link-to-a-commit)
  - [Link to a repo line](#link-to-a-repo-line)
  - [wiki Includes](#wiki-includes)
  - [Textile collapse](#textile-collapse)
  - [Macros](#macros)
  - [Textile escape](#textile-escape)
- [xclip](#xclip)
  - [Add file content to clipboard](#add-file-content-to-clipboard)
- [cat](#cat)
  - [cat \> lxd-init.yaml \<\<EOF](#cat--lxd-inityaml-eof)
- [netstat](#netstat)
  - [netstat -putona](#netstat--putona)
  - [netstat -capullo](#netstat--capullo)
- [date](#date)
  - [| tee ~/treball//logs/tinkiservice\_`date +"%Y%m%d%H%M%S"`.log](#-tee-treballlogstinkiservice_date-ymdhmslog)
  - [ssh](#ssh)
    - [ssh remote execute](#ssh-remote-execute)
    - [Jump host](#jump-host)
- [postgres](#postgres)
  - [psql -U tinki -h localhost](#psql--u-tinki--h-localhost)
  - [\\l](#l)
  - [psql -U tinki -h localhost -l](#psql--u-tinki--h-localhost--l)
  - [\\du](#du)
  - [\\dt](#dt)
  - [select table\_name,column\_name,data\_type from information\_schema.columns where table\_name='color';](#select-table_namecolumn_namedata_type-from-information_schemacolumns-where-table_namecolor)
  - [psql -U tinki -h localhost -c "select ..."](#psql--u-tinki--h-localhost--c-select-)
  - [No pager on psql -U tinki -h localhost -c "select ..."](#no-pager-on-psql--u-tinki--h-localhost--c-select-)
  - [docker exec -ti aaaservice\_postgres\_1 psql -U tinki](#docker-exec--ti-aaaservice_postgres_1-psql--u-tinki)
  - [create database test;](#create-database-test)
  - [create user test\_user with encrypted password 'pepepe';](#create-user-test_user-with-encrypted-password-pepepe)
  - [grant all privileges on database test to test\_user;](#grant-all-privileges-on-database-test-to-test_user)
  - [.pgpass](#pgpass)
- [pytest](#pytest)
  - [pytest -s](#pytest--s)
  - [pytest](#pytest-1)
  - [pytest test/\<someTest\>](#pytest-testsometest)
  - [pytest test/\<someTest\> -k ](#pytest-testsometest--k-)
  - [pytest -s test/\<someTest\> -k  --capture=no](#pytest--s-testsometest--k----captureno)
  - [pytest with tee](#pytest-with-tee)
  - [pytest fail](#pytest-fail)
  - [mock.patch](#mockpatch)
  - [pytest logglevel=debug](#pytest-loggleveldebug)
  - [Dummy test](#dummy-test)
- [openssl](#openssl)
  - [echo | openssl s\_client -servername api.loxonecloud.com -connect api.loxonecloud.com:443 2\>/dev/null | openssl x509 -noout -dates](#echo--openssl-s_client--servername-apiloxonecloudcom--connect-apiloxonecloudcom443-2devnull--openssl-x509--noout--dates)
- [vi](#vi)
  - [Comentar més d'una línia](#comentar-més-duna-línia)
  - [Copy to external clipboard](#copy-to-external-clipboard)
- [tar](#tar)
  - [tar create (1cpu)](#tar-create-1cpu)
  - [tar create (all cpus)](#tar-create-all-cpus)
- [k8s - Kubernetes](#k8s---kubernetes)
  - [Service concepts](#service-concepts)
    - [ClusterIP](#clusterip)
    - [NodePort](#nodeport)
    - [LoadBalancer](#loadbalancer)
    - [ExternalName](#externalname)
  - [Cheatsheets](#cheatsheets)
  - [Common pod operations](#common-pod-operations)
  - [K8s cluster test](#k8s-cluster-test)
  - [Kubectl bash autocompletion](#kubectl-bash-autocompletion)
    - [kubectl get all --show-labels](#kubectl-get-all---show-labels)
    - [kubectl get --raw='/readyz?verbose' ~ componentstatus](#kubectl-get---rawreadyzverbose--componentstatus)
    - [kubectl get nodes -o wide](#kubectl-get-nodes--o-wide)
    - [crictl ps](#crictl-ps)
    - [ip ad](#ip-ad)
    - [kubectl get pods --all-namespaces](#kubectl-get-pods---all-namespaces)
    - [kubectl cluster-info](#kubectl-cluster-info)
  - [k8s destroy node](#k8s-destroy-node)
  - [K8s commands](#k8s-commands)
    - [kubectl config current-context](#kubectl-config-current-context)
    - [kubectl get node --as developer](#kubectl-get-node---as-developer)
    - [kubectl get node --as admin](#kubectl-get-node---as-admin)
    - [kubectl explain deployment.spec](#kubectl-explain-deploymentspec)
    - [kubectl explain deployment.metadata](#kubectl-explain-deploymentmetadata)
    - [kubectl explain svc.spec](#kubectl-explain-svcspec)
    - [Dryrun](#dryrun)
  - [kubectl get nodes](#kubectl-get-nodes)
  - [kubectl get all -l app=nginx](#kubectl-get-all--l-appnginx)
  - [kubectl get all --show-labels](#kubectl-get-all---show-labels-1)
  - [kubectl get all -o wide](#kubectl-get-all--o-wide)
  - [kubectl get po --all-namespaces](#kubectl-get-po---all-namespaces)
  - [kubectl get po](#kubectl-get-po)
  - [kubectl get namespace](#kubectl-get-namespace)
  - [kubectl get po share-pod -o json](#kubectl-get-po-share-pod--o-json)
  - [kubectl delete -f webserver-svc.yaml](#kubectl-delete--f-webserver-svcyaml)
  - [kubectl delete app=nginx](#kubectl-delete-appnginx)
  - [kubectl create -f share-pod.yaml](#kubectl-create--f-share-podyaml)
  - [kubectl get pods -l app=share-pod -o jsonpath="{.items\[*\].spec.containers\[*\].image}" |tr -s '\[\[:space:\]\]' '\\n' |sort |uniq -c](#kubectl-get-pods--l-appshare-pod--o-jsonpathitemsspeccontainersimage-tr--s-space-n-sort-uniq--c)
  - [kubectl describe pod/share-pod](#kubectl-describe-podshare-pod)
  - [kubectl exec -it  -c  -- bash](#kubectl-exec--it---c-----bash)
  - [kubectl run -it --rm --restart=Never busybox --image=gcr.io/google-containers/busybox sh](#kubectl-run--it---rm---restartnever-busybox---imagegcriogoogle-containersbusybox-sh)
  - [kubectl create configmap my-config --from-literal=key1=value1 --from-literal=key2=value2](#kubectl-create-configmap-my-config---from-literalkey1value1---from-literalkey2value2)
  - [yaml for creation configmap](#yaml-for-creation-configmap)
  - [kubectl get configmaps my-config](#kubectl-get-configmaps-my-config)
  - [kubectl get configmaps my-config -o json](#kubectl-get-configmaps-my-config--o-json)
  - [Navigate to namespace](#navigate-to-namespace)
  - [ingress pods in ingress-nginx](#ingress-pods-in-ingress-nginx)
  - [List all pods versions (controllers versions)](#list-all-pods-versions-controllers-versions)
  - [minikube version](#minikube-version)
  - [Kubernetes logs](#kubernetes-logs)
  - [Test if a service account can do certain action which will be in the below format and the result will be yes or no](#test-if-a-service-account-can-do-certain-action-which-will-be-in-the-below-format-and-the-result-will-be-yes-or-no)
  - [Test if a user account can do certain action](#test-if-a-user-account-can-do-certain-action)
  - [Kubernetes top](#kubernetes-top)
    - [Top pods in CPU and memory](#top-pods-in-cpu-and-memory)
  - [Kubernetes cp](#kubernetes-cp)
  - [Kubernetes abbreviations](#kubernetes-abbreviations)
  - [Kubernetes drain](#kubernetes-drain)
- [Libvirt](#libvirt)
  - [List vm for all domains](#list-vm-for-all-domains)
  - [Start Libvirt network default](#start-libvirt-network-default)
  - [Connect console](#connect-console)
  - [List assigned IPs](#list-assigned-ips)
  - [Expiry Time           MAC address         Protocol   IP address           Hostname        Client ID or DUID](#expiry-time-----------mac-address---------protocol---ip-address-----------hostname--------client-id-or-duid)
  - [Start all VMs](#start-all-vms)
  - [libvirt restart](#libvirt-restart)
  - [libvirt logs](#libvirt-logs)
- [nmap](#nmap)
  - [Fast scan: nmap -sn 192.168.1.0/24](#fast-scan-nmap--sn-1921681024)
- [ssh](#ssh-1)
  - [ssh execute](#ssh-execute)
- [Flask](#flask)
  - [Blueprint dummy\_test](#blueprint-dummy_test)
  - [Blueprint default route](#blueprint-default-route)
  - [debug mode run](#debug-mode-run)
  - [debug and development mode](#debug-and-development-mode)
  - [Debugging with vscode (No docker)](#debugging-with-vscode-no-docker)
- [Suport als projectes](#suport-als-projectes)
  - [Matriu RACI](#matriu-raci)



# Ansible
## Execute command in multiple servers
ansible pro_docker -m command -a "date +%H:%M:%S_%N"

ansible pre_db,pro_db -m ping



## Playbooks
ansible-playbooh -i su0472 mariadb/tasks/main.yml --check
```
 -i, --inventory, --inventory-file
    specify inventory host path or comma separated host list
 --c
```

ansible-playbook -v --check -i su0472 -t auditç

ansible-playbook -v playbooks/pre.yml --tags mariadb

ansible-playbook playbooks/lab.yml --check --limit lab --tags users_to_remove

ansible-playbook playbooks/pro.yml --limit=pro_docker --tag docker_swarm --check

ansible-playbook playbooks/run_nginx_test_pro.yml --limit pro_adm --check

ansible-playbook playbooks/start-replication.yml -e host_master=su0476 -e host_slave=su0472

ansible-playbook playbooks/pre.yml --limit su0472 --tags maxscale --check

ansible-playbook playbooks/bootstrap-base.yml --limit pre_db --check

ansible-playbook playbooks/bootstrap-base.yml --limit su0476 --tag maxscale_run

ansible-playbook -i inventory/all -l pre_db playbooks/pre.yml --tags audit --check -v

ansible-playbook -l pre_db playbooks/pre.yml --tags audit --check -v

ansible-playbook -l su0472 playbooks/pre.yml --tags audit --check -v

## playbook with multiple tags
ansible-playbook <playbookFile> -tags <tag1>,<tag2>

## index in a loop
```yaml
---
- hosts: all
  gather_facts: no
  tasks:
    - name: print result
      debug:
        msg: "{{ indice }}. El usuario {{ item.nombre }} pertenence a {{ item.grupo }}"
      loop:
        - {nombre: "lorenzo", grupo: "usuarios"}
        - {nombre: "jose", grupo: "usuarios"}
        - {nombre: "fermin", grupo: "administradores"}
        - {nombre: "alberto", grupo: "administradores"}
      loop_control:
        index_var: indice
        pause: 1
      register: result
    - name: echo
      debug:
        msg: "{{ result }}"
```

## Index and conditionals
```yaml
---
- hosts: all
  gather_facts: no
  tasks:
    - name: print result
      debug:
        msg: "{{ indice }}. El usuario {{ item.nombre }} pertenence a {{ item.grupo }}"
      register: result
      when:
        - indice > 1
        - indice < 4
      loop:
        - {nombre: "lorenzo", grupo: "usuarios"}
        - {nombre: "jose", grupo: "usuarios"}
        - {nombre: "fermin", grupo: "administradores"}
        - {nombre: "alberto", grupo: "administradores"}
      loop_control:
        index_var: indice
        pause: 1
        label: "{{ item.nombre }}"
    - name: echo
      debug:
        msg: "{{ result }}"
```

## Blocks
```yaml
---
- hosts: all
  order: sorted
  gather_facts: yes

  tasks:
    - name: Lista un directorio según la distribución
      block:
      - name: lista el directorio raíz
        shell: ls -la /
        register: result
      - name: print result
        debug:
          msg: "{{ result }}"
      when: (ansible_facts.distribution == 'Ubuntu') and
            (ansible_facts.os_family == 'Debian')
      ignore_errors: yes
      remote_user: root
```

## Blocks and error management
```yaml
---
- hosts: all
  order: sorted
  gather_facts: yes

  tasks:
    - name: Hace un ping en algunos casos
      block:
      - name: lista el directorio /etz
        shell: ls -la /etz
        register: result
      - name: print result
        debug:
          msg: "{{ result }}"
      rescue:
        - name: en caso de error
          debug:
            msg: "He capturado un error"
        - name: lista el directorio /etc
          shell: ls -la /etc | tail -1
          register: result
        - name: print result
          debug:
            msg: "{{ result.stdout }}"
      when: (ansible_facts.distribution == 'Ubuntu') and
            (ansible_facts.os_family == 'Debian')
      ignore_errors: no
      remote_user: root
```


# bash

## 5 Uses of Set Commands To Make Your Bash Scripts Secure and Robust
https://medium.com/techtofreedom/5-uses-of-set-commands-to-make-your-bash-scripts-secure-and-robust-a747a4e79be2

Added to [bash shellscript best practices - ‘set’ builtins — fail fast](#bash-shellscript-best-practices---set-builtins--fail-fast)

* set -u o set -o nounset -> Detect Non-Existing Variables
* set -o xtrace -> Print a Command Before Displaying Its Outputs


## bash shellscript best practices - trap handler
https://betterprogramming.pub/best-practices-for-bash-scripts-17229889774d

```bash
function handle_exit() {
  // Add cleanup code here
  // for eg. rm -f "/tmp/${lock_file}.lock"
  // exit with an appropriate status code
}

// trap <HANDLER_FXN> <LIST OF SIGNALS TO TRAP>
trap handle_exit 0 SIGHUP SIGINT SIGQUIT SIGABRT SIGTERM
```

## bash shellscript best practices - ‘set’ builtins — fail fast
https://betterprogramming.pub/best-practices-for-bash-scripts-17229889774d

```bash
#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

function print_var() {
  echo "${var_value}"
}

print_var
```


## bash command combinations
* command1 && command2: Executes command1 first. Then, executes command2 only if command1 didn’t return a non-zero exit code
* command1 || command2: Executes command1. Then, executes command2 only if command1 returns a non-zero exit code
* command1 | command2: Executes both commands sequentially by sending command1‘s standard output to command2‘s standard input
* command1 |& command2: Works similarly as the | operator, but sends both standard output and error data to command2
* command1 ; command2: Executes both commands sequentially regardless of process exit codes (As the same as the return key)
* command1 & command2 &: Executes both commands as background jobs


## bash calculator
https://www.geeksforgeeks.org/bc-command-linux-examples/


bc -l
```yaml
10^2
100
```

villaraco@db:~$ echo "var=10;var^=2;var" | bc
```yaml
100
```

$ echo "obase=16; ibase=10; 1000;" | bc
3E8




## Bash: execute a script only when no other instance of the script is running
https://lovethepenguin.com/bash-execute-a-script-only-when-no-other-instance-of-the-script-is-running-283e7d726547

```shell
[ "$(pidof -x $(basename $0))" != $$ ] && exit
```

How this cute one-liner works

    $$ : Gets the current bash shell PID, $$ in a bash script is the scripts PID
    $0 : The name of the script
    basename : prints the last part of a filename
    pidof -x : Gets the PIDs of $(basename $0) it return many PIDs if the script runs more than once.
    && exit if the previous statement returns 0 exits


## Bash Hash Table/Dictionary
https://levelup.gitconnected.com/5-less-known-bash-concepts-to-level-up-your-linux-skills-7bcf363804d1


declare -A myHashTable

myHashTable[a]='Ashish'

myHashTable[b]='Bamania'

for key in "${!myHashTable[@]}"
> do
>   echo -n "key: $key, "
>   echo "value: ${myHashTable[$key]}"
> done





## bash arrays
https://levelup.gitconnected.com/5-less-known-bash-concepts-to-level-up-your-linux-skills-7bcf363804d1


array=('Ashish' 1 3 'b')

array_length=${#array[@]}

echo ${array[*]} -> Print all array

echo ${array[@]} -> Print all array

echo ${array[0]} -> Print a particular index

array[5]='cat'

for elem in ${!array[@]}; do
  echo "Element $elem is ${array[$elem]}"
done




## Webserver
https://lovethepenguin.com/linux-create-a-one-line-web-server-in-bash-that-will-execute-shell-commands-98838e429fb

echo 'while true; do echo -e "HTTP/1.1 200 OK\n\n$($1)" | nc -l -k -p 8080 -q 1; done' > webserver.sh

chmod 744 webserver.sh

./webserver.sh "hostname"

curl 127.0.0.1:8080
```yaml
db2000778
```

## Show animation while executing a long time task
https://levelup.gitconnected.com/5-modern-bash-scripting-techniques-that-only-a-few-programmers-know-4abb58ddadad

```bash
#!/bin/bash

sleep 5 &
pid=$!
frames="/ | \\ -"
while kill -0 $pid 2&>1 > /dev/null;
do
    for frame in $frames;
    do
        printf "\r$frame Loading..." 
        sleep 0.5
    done
done
printf "\n"
```


## Multiprocessing in bash
https://levelup.gitconnected.com/5-modern-bash-scripting-techniques-that-only-a-few-programmers-know-4abb58ddadad

```bash
#!/bin/bash

function task1() {
    echo "Running task1..."
    sleep 5
}

function task2() {
    echo "Running task2..."
    sleep 5
}

task1 &
task2 &

wait

echo "All done!"
```

## Colors & styles in term
https://levelup.gitconnected.com/5-modern-bash-scripting-techniques-that-only-a-few-programmers-know-4abb58ddadad

``` bash
#!/bin/bash
bold=$(tput bold)
underline=$(tput smul)
italic=$(tput sitm)
info=$(tput setaf 2)
error=$(tput setaf 160)
warn=$(tput setaf 214)
reset=$(tput sgr0)

echo "${info}INFO${reset}: This is an ${bold}info${reset} message"
echo "${error}ERROR${reset}: This is an ${underline}error${reset} message"
echo "${warn}WARN${reset}: This is a ${italic}warning${reset} message"
```

## Readonly variables (constants)
https://aoyilmaz.medium.com/bash-scripting-for-beginners-part-1-introduction-and-variables-638f3d52ee71

#!/bin/bash

NAME="Ahmet Okan YILMAZ"
readonly NAME
NAME="Selim Batur YILMAZ"


## Standard variables
https://aoyilmaz.medium.com/bash-scripting-for-beginners-part-1-introduction-and-variables-638f3d52ee71

    $0: The name of the Bash script.
    $n: n is a positive deciamal number. $1 stands for first argument.
    $#: How many arguments were passed to the Bash script.
    $@: All the arguments supplied to the Bash script.
    $?: The exit status of the most recently run process.
    $$: The process ID of the current script.
    $!: The process number of the last background command.
    $USER: The username of the user running the script.
    $HOSTNAME: The hostname of the machine the script is running on.
    $SECONDS: The number of seconds since the script was started.
    $RANDOM: Returns a different random number each time is it referred to.
    $LINENO : Returns the current line number in the Bash script.

## for loop
https://www.cyberciti.biz/faq/bash-for-loop/

for i in {1..3}; do echo "loop #${i}"; done

## sed
Output to stdout:
sed -E "s/^(.*)$/cat \/dir\/\1\/html\/file.yml/" file.txt

Infile:
sed -i -E "s/^(.*)$/cat \/dir\/\1\/html\/file.yml/" file.txt

sed -n -e 1,2p somefile.txt

sed -n -e 1,2p -e 4p somefile.txt
    Line 1
    Line 2
    Line 4

## if
```bash
#!/bin/bash
# Basic if statement
if [ $1 -gt 100 ]
then
    echo Hey that\'s a large number.
    pwd
fi
date
```

    Operator 	Description
    ! EXPRESSION 	The EXPRESSION is false.
    -n STRING 	The length of STRING is greater than zero.
    -z STRING 	The lengh of STRING is zero (ie it is empty).
    STRING1 = STRING2 	STRING1 is equal to STRING2
    STRING1 != STRING2 	STRING1 is not equal to STRING2
    INTEGER1 -eq INTEGER2 	INTEGER1 is numerically equal to INTEGER2
    INTEGER1 -gt INTEGER2 	INTEGER1 is numerically greater than INTEGER2
    INTEGER1 -lt INTEGER2 	INTEGER1 is numerically less than INTEGER2
    -d FILE 	FILE exists and is a directory.
    -e FILE 	FILE exists.
    -r FILE 	FILE exists and the read permission is granted.
    -s FILE 	FILE exists and it's size is greater than zero (ie. it is not empty).
    -w FILE 	FILE exists and the write permission is granted.
    -x FILE 	FILE exists and the execute permission is granted.

## Memory leak
ps -eo pmem,pcpu,rss,vsize,args | sort -k 1 -n -r | head

ps aux --sort=-%mem | head

## bash pretty printing
```bash
| python -m json.tool
```

## Change symblink
ln -vfns ~/Documents/network/device/ASUSDF-3760/config/ Router

## Count hit per second NGINX
zgrep -e "Nov 17 12:28:[23]" /var/log/docker_swarm/nginx.log-20211118.gz | cut -d ' ' -f3 | uniq -c

## Aliases and functions (.bashrc)

```yaml
alias aPt='apt update;apt list --upgradable'

# Afegit pel JGVP a 22.02.18
alias i='iptables -L -n'
alias dI='docker images'
alias dP='docker ps'
alias dPa='docker ps -a'
alias dS='docker service ls'

alias ipE='curl ifconfig.co'

alias dI='docker images'
alias dP='docker ps'
alias dPa='docker ps -a'
alias dS='docker service ls'
export EDITOR=vi

export LIBVIRT_DEFAULT_URI='qemu:///system'
alias vN='virsh net-list --all'
# IPs assignades a m??quines
alias vNa='virsh net-dhcp-leases default'
alias vL='virsh list --all'
alias vD='ll /var/lib/libvirt/images/'
alias pw5="dd if=/dev/urandom bs=5 count=1 2>/dev/null | base64 | sed 's/=//g'"
alias pw8="dd if=/dev/urandom bs=8 count=1 2>/dev/null | base64 | sed 's/=//g'"
alias pw16="dd if=/dev/urandom bs=16 count=1 2>/dev/null | base64 | sed 's/=//g'"

alias aM="encfs ~/treball/.secret_e ~/treball/secret; mount | grep /home/user/treball/secret"
alias aU="fusermount -u ~/treball/secret"

alias gS='git status'

# Kubernetes
alias kNs='kubectl get namespaces --show-labels'

function kubectlgetall {
  for i in $(kubectl api-resources --verbs=list --namespaced -o name | grep -v "events.events.k8s.io" | grep -v "events" | sort | uniq); do
    echo "Resource:" $i
    kubectl -n ${1} get --ignore-not-found ${i}
  done
}

function kGn() { kubectl get all,pv,pvc,ingress -n "$1"; }

alias kCa='kubectl config set-context --current --namespace=app'

```

## bash expansion
touch f{1,2,3,4}

touch f{1..4}


## bash echoing every line
```bash
#!/bin/bash
set -x #echo on

ls $PWD
```


## aliases in .bashrc

```shell
# 07.03.2023 from https://aruva.medium.com/100-bash-aliases-for-supersonic-productivity-d54a796422d9
alias ..='cd ..'
alias grep='grep --color=auto'
alias c='clear'
alias h='history'
alias mv='mv -i'
alias cp='cp -i'
alias ports='netstat -a | grep -i "listen"'
alias lr='ls -ltr'
alias path='echo -e ${PATH//:/\\n}'
alias tree='tree -C --dirsfirst'
alias publicip='curl ifconfig.me'
alias weather='function _weather() { curl wttr.in/$1; }; _weather'
alias temps='weather barcelona'
alias jsonpretty='function _jsonpretty() { python -m json.tool $1; }; _jsonpretty'
alias rename='function _rename() { for i in *$1*; do mv "$i" "${i/$1/$2}"; done }; _rename'
alias encrypt='function _encrypt() { openssl enc -aes-256-cbc -salt -in $1 -out $2; }; _encrypt' # This encrypts a file using AES-256 encryption
alias decrypt='function _decrypt() { openssl enc -d -aes-256-cbc -in $1 -out $2; }; _decrypt' # This decrypts a file that was encrypted using AES-256 encryption
```



# curl
## No progress meter (-s)
## Check service health
curl -Is http://www.google.com

    The -s option is used for quiet mode, which disables the progress meter and the error messages.
    The -I option sends a HEAD HTTP request and doesn’t return the request body.
    
## Test telnet functionality
curl -v telnet://127.0.0.1:8090

## "Host:" header
curl -H "Host: example.com" http://localhost/

## Follow redirects
-L 

## Supress output
curl -vs -o /dev/null <URL>



# Debian
## backports config
apt edit-sources

deb http://deb.debian.org/debian bullseye-backports main contrib non-free

## Find packages in all repos
apt show libreoffice -a

## install from backports
apt -t bullseye-backports install libreoffice



# Diagram as a code
* PlantUML
* Diagrams https://diagrams.mingrammer.com/


# dig
## Get GLUE NS Records
```
dig +norec +noquestion +nocomments +nocmd +nostats @ns.nic.cat. calders.cat. NS
```


# Docker
## Running as non-root user
  ```dockerfile
  FROM mcr.microsoft.com/dotnet/aspnet:5.0  # Create group app and user app
  RUN addgroup --gid 1000 -S app && adduser --uid 1000 -S app -G app  # Set permissions for app user for the WORKDIR    
  RUN chown -R app:app App/  # Switch to the created user
  USER app
 
  COPY bin/Release/netcoreapp3.1/publish/ App/
  WORKDIR /App
  ENTRYPOINT ["dotnet", "aspnetapp.dll"]
  ```


## docker rm -f $(docker ps -a -q)
remove all containers

## docker system prune -f
Deletes everything: networks, containers, images, volumes ...

## Create efimer debian docker container daemon
docker run --rm --detach --tty --name debian11 debian:11-slim

or 

docker run --rm -ti --name debian11 debian:11-slim /bin/bash

or

docker exec -it debian11 bash

(In order to test a other containers):
apt install curl inetutils-ping


# Docker Swarm
## Nodes info
docker node ls

## Services in a node
docker service ls

## Service info
docker service ps <service>

## Forced service reload
docker service update nginx --force


# Docker composer
## Start up all services
./docker-componse up 

## Start up all services in dettached mode
./docker-componse up -d



# find
## Find 50 biggest files
find  -type f  -exec  du -Sh {} +  |  sort -rh  |  head -n 50

sed -n -e 1,2p -e 4p somefile.txt
    Line 1
    Line 2
    Line 4

## Using find to grep on just a selected files
find . -name "diba*.md" -exec grep -H -n -i X-Redmine-API-Key {} \;
    grep -H: -H, --with-filename       print file name with output lines
    grep -n: -n, --line-number         print line number with output lines


## Using find with xargs
find . -type f -name "personal*.md" -print0 | xargs -0 grep --color=auto -n -i 'searchedString'

find . -type f -name "*.md" -print0 | xargs -0 grep --color=auto -n -i -e '##.*correu'

find ~/Dropbox -type f -name "*.md" -print0 | xargs -0 grep --color=auto -n -i -e '##.*correu'

  -print0
              True; print the full file name on the standard output, followed by a null character (instead of the newline character that -print uses).  This allows  file  names  that
              contain newlines or other types of white space to be correctly interpreted by programs that process the find output.  This option corresponds to the -0 option of xargs.
  xargs
      This  manual page documents the GNU version of xargs.  xargs reads items from the standard input, delimited by blanks (which can    be protected with double or single quotes or a
       backslash) or newlines, and executes the command (default is /bin/echo) one or more times with any initial-arguments followed by items read from standard input.   Blank  lines
       on the standard input are ignored.

  -0, --null
              Input items are terminated by a null character instead of by whitespace, and the quotes and backslash are not special (every character is  taken  literally).   Disables
              the end of file string, which is treated like any other argument.  Useful when input items might contain white space, quote marks, or backslashes.  The GNU find -print0
              option produces input suitable for this mode.
  




# GIT
## git code operations types
https://medium.com/patilswapnilv/10-git-best-practices-to-start-using-in-your-next-git-commit-5a43ae646e91

feat

    Title: Feature
    Description: A completely new feature
    Emoji: ✨

fix

    Title: Bug Fix
    Description: A bug fix
    Emoji: 🐛

docs

    Title: Documentation
    Description: Changes only to the documentation
    Emoji: 📚

style

    Title: Styles
    Description: Alterations that don’t change the code’s intent(white space, formatting, missing semi-colons, etc.)
    Emoji: 💎

refactor

    Title: Code Refactoring
    Description: An update to the code that neither adds features nor fixes bugs
    Emoji: 📦

perf

    Title: Performance Improvements
    Description: A performance-enhancing tweak to the code
    Emoji: 🚀

test

    Title: Tests
    Description: Addition of tests or updating of tests
    Emoji: 🚨

build

    Title: Builds
    Description: Modifications to the build system or external dependencies
    Emoji: ⚒️

ci

    Title: Continuous Integrations
    Description: Modifications to our CI configuration scripts and files
    Emoji: ⚙️

chore

    Title: Chores
    Description: Other updates that don’t alter the test or src files
    Emoji: ♻️

revert

    Title: Reverts
    Description: Reverts a previous commit
    Emoji: 🗑

Now, let’s see some commit aliases:
initial

    Maps to: feat
    Title: Initial
    Description: Initial Commit
    Emoji 🎉

dependencies

    Maps to: fix
    Title: Dependencies
    Description: Update dependencies
    Emoji: ⏫

peerDependencies

    Maps to: fix
    Title: Peer dependencies
    Description: Update peer dependencies
    Emoji: ⬆️

devDependencies

    Maps to: chore
    Title: Dev dependencies
    Description: Update development dependencies
    Emoji: 🔼

metadata

    Maps to: fix
    Title: Metadata
    Description: Update metadata (package.json)
    Emoji: 📦


## .gitignore template
```yaml
__pycache__/
```

## git remote show origin
    Mostra els origens ...


## git clone --single-branch --branch <branchname> host:/dir.git
    Clonat d'una sola branca del servidor

## git clone --single-branch --branch HAL6 \<server\>:/servers/servers.git


## git branch
    A quina branca estic?

## git branch my2.6.14 v2.6.14
    Crea branca my.2.6.14 i assigna versió 2.6.14(?)

## git checkout my2.6.14
    Situa el codi a la branca my2.6.14

## git branch --set-upstream-to=origin/\<branca\>
    Configura l'origen de la branca local a origin/<branca>

## git commit -m "\<text\>"
    Commit amb el comentari <text>


## Merge a master de la branca:
git checkout master

git merge \<nomBranca\>
```
    Updating f42c576..3a0874c
    Fast-forward
    index.html | 2 ++
    1 file changed, 2 insertions(+)
```

git status
 ```
    On branch master
    Your branch is ahead of 'origin/master' by 2 commits.
    (use "git push" to publish your local commits)
    nothing to commit, working tree clean
 ```

git push
```
    Total 0 (delta 0), reused 0 (delta 0)
    remote: . Processing 1 references
    remote: Processed 1 references in total
    To ssh://\<server\>/joan.g.villaraco/certsManager.git
    14c173a..c80a705  master -> master
```



## Merge de branca \<branca\> a partir del master:
    OJU!!!! que aquesta és l'operació contraria a la normal!!!
    git checkout master
    <edits>
    git add, commit, push

    git checkout \<branca\>
    git merge master
    git push


## git log
    Mostra els commits fets en local, només metadades


## git log -p
    Mostra els commits amb info de detall (canvis)


## git diff
    Previ al commit mostra els canvis detallats


## git push -f origin <last_known_good_commit>:<branch_name>


## git push -f origin db773cff80:\<branca\>
    Passar al commit XXX la branca \<branca\> del servidor

## git push -f 
    Forçar la situació local a l'origin. Normalment per a tirar enrera canvis pujats a l'origen per error i deprés d'haver-los solucionats al local.

## git push --set-upstream origin TIN-1986_backend_test_if_create_method_is_necessary
    Crea la nova branca TIN-*** a origin i puja el codi

## Crear repositori inicial:
    Crear repo al server
    touch README.md
    git init
    git add README.md
    git commit -m "first commit"
    git remote add origin ssh://git@home.\<server\>.com:24862/joan.g.villaraco/test01.git
    git push -u origin master

## git checkout -- public/bin/control/README.md
    Sense haver fet el commit, torna el fitxer a l'estat previ al canvi

## Git rebase
    git checkout master
    git pull
    git checkout feature-xyz  # name of your hypothetical feature branch
    git rebase master  # may need to fix merge conflicts in feature-xyz

## git pull origin master
    Pull del master d'origen a una branca diferent de la master en local

## git reset --hard
Tira enrera canvis en local fins al darrer commit

## git reset --hard origin/master
Tira enrera els canvis locals i posa els arxius locals com a origin.master. També pot ser origin/\<branca\>.

## git reset --soft HEAD@{1}
Tira enrera els canvis a l'últim commit previ al HEAD. Deixa els canvis a a la stagging àrea, si es vol tornar eliminar també els canvis de la stagging àrea per el ```git reset --hard```

### Prova:
git commit -m "commit1"
```
    [testBacktoPreviousCommit c0d036f] commit1
    1 file changed, 2 insertions(+)
    create mode 100644 prova.txt
```

git status
```
    On branch testBacktoPreviousCommit
    nothing to commit, working tree clean
```

git log
```
    commit c0d036f5b0fb28d54d306552541f82abb36d2694 (HEAD -> testBacktoPreviousCommit)
    Author: Joan G. Villaraco <joan.g.villaraco@<server>.com>
    Date:   Tue Aug 18 11:34:12 2020 +0200


        commit1

    commit c80a7057ec224039d136a5d169a71aad53de8897 (origin/master, origin/00083_stationStatusMultiErrorButUptime100, master, 00083_stationStatusMultiErrorButUptime100)
    Author: Joan G. Villaraco <joan.g.villaraco@<server>.com>
    Date:   Tue Aug 18 11:24:23 2020 +0200

        bug #83 cameras CATCAM1..3: repaired bug

    commit b05995c7195187a1f4f32b2d4fac06c856c650ba
    Author: Joan G. Villaraco <joan.g.villaraco@<server>.com>
    Date:   Mon Aug 17 12:57:32 2020 +0200

        #81 cameras CATCAM1..3: test sqlalchemy union

    commit 14c173a150dffa0a984c5302e4d7f2ccac400248
    Author: Joan G. Villaraco <joan.g.villaraco@<server>.com>
    Date:   Mon Aug 17 11:21:59 2020 +0200

        Update README.md to test html anchors for linking
    ...

git reset --soft HEAD@{1}

git log
    commit c80a7057ec224039d136a5d169a71aad53de8897 (HEAD -> testBacktoPreviousCommit, origin/master, origin/00083_stationStatusMultiErrorButUptime100, master, 00083_stationStatusMultiErrorButUptime100)
    Author: Joan G. Villaraco <joan.g.villaraco@<server>.com>
    Date:   Tue Aug 18 11:24:23 2020 +0200

        bug #83 cameras CATCAM1..3: repaired bug

    commit b05995c7195187a1f4f32b2d4fac06c856c650ba
    Author: Joan G. Villaraco <joan.g.villaraco@<server>.com>
    Date:   Mon Aug 17 12:57:32 2020 +0200

        #81 cameras CATCAM1..3: test sqlalchemy union

git status
    On branch testBacktoPreviousCommit
    Changes to be committed:
    (use "git reset HEAD <file>..." to unstage)

        new file:   prova.txt

git reset --hard
    HEAD is now at c80a705 bug #83 cameras CATCAM1..3: repaired bug

git status
    On branch testBacktoPreviousCommit
    nothing to commit, working tree clean
```

## git show main:README.md
Shows README.md from main branch


## git rev-list --all | xargs git grep -F ‘font-size: 52 px;’
Search in all branchs for font-size: 52 px;


## git ssh key
ssh-keygen -t rsa -C "joangvillaraco_<server>"

## git config --global
~/.gitconfig

## git config (local)
git config user.email jvillaraco@gmail.com
git config user.name "Joan G. Villaraco"

On a basic auth git, in order to automate username, edit .git/config:
```
...
[remote "origin"]
	url = https://<username>@<remoteURL>
...
```

```git config -l```
```
core.repositoryformatversion=0
core.filemode=true
core.bare=false
core.logallrefupdates=true
remote.origin.url=https://****
remote.origin.fetch=+refs/heads/*:refs/remotes/origin/*
branch.master.remote=origin
branch.master.merge=refs/heads/master
user.email=gvillaracoj@diba.cat
user.name=Joan G. Villaraco
```

## Get local track of a remorte branch

git checkout --track origin/<remoteBranch>

when trying to checkout a remote branch, you get this warning:

```git checkout remotes/origin/<remoteBranch>```

    Note: checking out 'remotes/origin/<remoteBranch>'.

    You are in 'detached HEAD' state. You can look around, make experimental
    changes and commit them, and you can discard any commits you make in this
    state without impacting any branches by performing another checkout.

    If you want to create a new branch to retain commits you create, you may
    do so (now or later) by using -b with the checkout command again. Example:

    git checkout -b <new-branch-name>

    HEAD is now at b1f54ef Netja del port 4008 Read Only als playbooks

To solve and make the branch editable use '--track'' flag:
https://www.git-tower.com/learn/git/faq/track-remote-upstream-branch

## Delete local & remote branch
Local branches:

git branch -d <branch-name>

git branch -D <branch-name> -> Force branch delete event there are unmerged changes

Remote branches:

git push --delete \<localbranch>

git push origin --delete \<remotebranch>

## synchronize your branch list
git fetch -p


## git stash
Save not stashed changes on local branch

git stash

git stash save -m "my work in progress"

git stash list

## Move uncommitted changes to new/existing branch
git switch -c new-branch


## Rewrite my commit text uploaded by error
https://opensource.com/article/22/11/git-tips-technical-writers

git commit --amend

git push --force


## I have accidentally committed to master
https://opensource.com/article/22/11/git-tips-technical-writers

Create a branch to save your work:

$ git switch -c my_feature

Switch back to the master branch:

$ git switch master

Drop the last commit on master:

$ git reset --soft HEAD~1

Switch back to my_feature branch and continue working:

$ git switch my_feature



# GraphViz
## Execució
dot -Tsvg ptgu.dot > ~/esborrar/ptgu.svg



# Helm
* https://www.tutorialworks.com/helm-cheatsheet/


## helm install --dryrun


## help install (sense repo)
```yaml
helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace
```

## helm list - List all deployed charts on my system



## helm show
## helm show chart


### helm show values
helm show values ingress-nginx/ingress-nginx


## helm status <release-name>


## helm upgrade


## helm repo
### heml repo add
helm repo add [NAME] [URL] [flags]

helm repo add metallb https://metallb.github.io/metallb

### help repo list
helm repo list
```yaml
NAME         	URL                                        
metallb      	https://metallb.github.io/metallb          
ingress-nginx	https://kubernetes.github.io/ingress-nginx 
projectcalico	https://projectcalico.docs.tigera.io/charts
```

## help install 
helm install [NAME] [CHART] [flags]

helm install metallb metallb/metallb -f ~/treball/k8s/metalLB/values.yaml


## helm uninstall - Deinstall
helm uninstall metallb


## Desplegar Help
https://helm.sh/docs/intro/install/#from-apt-debianubuntu


## Verificar helm
* https://www.tutorialworks.com/helm-cheatsheet/
* https://codefresh.io/docs/docs/new-helm/helm-best-practices/


# journalctl logs
## System boots
journalctl --list-boots

## service journal logs
journalctl -u openvpn.service


# Linux commands
## lsof 
lsof -u someuser
List open files by user

kill -9 `lsof -t -u someuser`
end all processes of a user

lsof -i :8090
processes running on a specific port


# Makefile
* https://medium.com/geekculture/devops-in-linux-makefile-part-one-a1634acf1ae0


# NGINX

systemctl restart certsmanager.service; systemctl status certsmanager.service


systemctl daemon-reload
    It has to be executed after changing /etc/systemd/system/certsmanager.service


systemctl status certsmanager.service


/etc/systemd/system/certsmanager.service


/etc/nginx/sites-available/certsmanager



# python
## Python command line

```python
$ python -c "x = 10; y = 20; z = str(x) + str(y); print(z)"
1020
```

```python
$ python -c "x = 10
> y = 20
> z = str(x) + str(y)
> print(z)"
1020
```

```python
$ echo "name = 'Andrew'" > names_module.py
$ python -c "import name_module; print(name_module.name)"
Andrew
```

## Python type hints
https://medium.com/techtofreedom/8-levels-of-using-type-hints-in-python-a6717e28f8fd

```yaml
name: str = "Yang"
age: int = 29
assets: list = []
posts: tuple = ()
hobbies: dict = {}
```

from typing import List


posts: list[str] = ['Python Type Hints', 'Python Tricks']


posts: dict[int, str] = {1: 'Python Type Hints', 2: 'Python Tricks'}


from typing import Final

DATABASE: Final = "MySQL"  ## Contants


def greeting(name: str) -> str:
    return "Hello " + name


from typing import Literal

weekends: Literal['Saturday', 'Sunday']



## Docs
* https://docs.python.org/release/3.9.2/


## Multithreading
https://python.plainenglish.io/mastering-python-the-10-most-difficult-concepts-and-how-to-learn-them-3973dd15ced4

```python
import threading

def worker():
    """thread worker function"""
    print(threading.get_ident())

threads = []
for i in range(5):
    t = threading.Thread(target=worker)
    threads.append(t)
    t.start()
```


## Maximum value of a list

```python
test = [1, 2, 3, 4, 2, 2, 3, 1, 4, 4, 4, 3, 1, 2, 2, 2]
print(max(set(test), key = test.count))
```


## print hacks
### clear a line after print it
```python
CURSOR_UP = '\033[1A'
CLEAR = '\x1b[2K'
CLEAR_LINE = CURSOR_UP + CLEAR

print('apple')
print('orange')
print('pear')

from time import sleep
sleep(1)

# clears TWO lines
print(CLEAR_LINE * 2, end='')
print('pineapple')
```

## Usefull decorators
### @lru_cache: Speed Up Your Programs by Caching
https://medium.com/techtofreedom/9-python-built-in-decorators-that-optimize-your-code-significantly-bc3f661e9017

```python
import time


def fibonacci(n):
    if n < 2:
        return n
    return fibonacci(n - 1) + fibonacci(n - 2)


start_time = time.perf_counter()
print(fibonacci(30))
end_time = time.perf_counter()
print(f"The execution time: {end_time - start_time:.8f} seconds")
# The execution time: 0.18129450 seconds
```

```python
from functools import lru_cache
import time


@lru_cache(maxsize=None)
def fibonacci(n):
    if n < 2:
        return n
    return fibonacci(n - 1) + fibonacci(n - 2)


start_time = time.perf_counter()
print(fibonacci(30))
end_time = time.perf_counter()
print(f"The execution time: {end_time - start_time:.8f} seconds")
# The execution time: 0.00002990 seconds
```

### @cached_property: Cache the Result of a Method as an Attribute
https://medium.com/techtofreedom/9-python-built-in-decorators-that-optimize-your-code-significantly-bc3f661e9017

```python
from functools import cached_property


class Circle:
    def __init__(self, radius):
        self.radius = radius

    @cached_property
    def area(self):
        return 3.14 * self.radius ** 2


circle = Circle(10)
print(circle.area)
# prints 314.0
print(circle.area)
# returns the cached result (314.0) directly
```

## Comprehesion / one liners
    some_list = []
    for i in range(10):
        some_list.append(i)

    # Simple List Comprehension
    some_list = [i for i in range(10)] 
    # Nested Loops
    some_list = [[i for i in range(10)] for j in range(10)]
    some_list = [i for i in range(10) for j in range(10)]
    # List Comprehension & Conditions
    some_list = [ x for x in range(20) if x % 2 == 0]
    some_list = [ y for y in range(20) if y != 2] 

new_list = [expression for member in iterable (if conditional)]

    >>> sentence = 'the rocket came back from mars'
    >>> vowels = [i for i in sentence if i in 'aeiou']
    >>> vowels
    ['e', 'o', 'e', 'a', 'e', 'a', 'o', 'a']

new_list = [expression (if conditional) for member in iterable]

    >>> original_prices = [1.25, -9.45, 10.22, 3.78, -5.92, 1.16]
    >>> prices = [i if i > 0 else 0 for i in original_prices]
    >>> prices
    [1.25, 0, 10.22, 3.78, 0, 1.16]

Sets

    >>> quote = "life, uh, finds a way"
    >>> unique_vowels = {i for i in quote if i in 'aeiou'}
    >>> unique_vowels
    {'a', 'e', 'u', 'i'}

Dicts

    >>> squares = {i: i * i for i in range(10)}
    >>> squares
    {0: 0, 1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49, 8: 64, 9: 81}


No item

    txns = [random.randrange(100) for _ in range(100000)]

If

```python
salary = 4000

if salary > 9000:
    tax = 900
elif 3000 < salary < 9000:
    tax = 100
else:
    tax = 0

print(f"Tax to pay : ${tax}")

tax = 900 if salary > 9000 else 100 if 3000 < salary < 5000 else 0

print(f"Tax to pay : ${tax}")

# Tax to pay : $100
# Tax to pay : $100
```



## time functions
### timeit
    import timeit
    timeit.timeit('x=(1,2,3,4,5,6,7,8,9)', number=100000)

0.0018976779974764213

    timeit.timeit('x=[1,2,3,4,5,6,7,8,9]', number=100000)

0.019868606992531568

### time
    import time

    start = time.time()

    def add(a, b):
    return a + b
    
    end_time = time.time()
    total_time = end_time - start
    print(total_time)

### time as a decorator

    >>> import time
    ... def logging_time(func):
    ...     """Decorator that logs time"""
    ...     def logger():
    ...         """Function that logs time"""
    ...         start = time.time()
    ...         func()
    ...         print(f"Calling {func.__name__}: {time.time() - start:.5f}")
    ... 
    ...     return logger
    ... 
    ... @logging_time
    ... def calculate_sum():
    ...     return sum(range(10000))
    ... 
    ... calculate_sum()
    ... 
    Calling calculate_sum: 0.00018

## Param
### Parametres amb nom i tipus, funció tipeada
def my_add(a: int, b: int) -> int:
    return a + b

### Passing multiples sets of args
 Python 3.5+ allows passing multiple sets
 of keyword arguments ("kwargs") to a
 function within a single call, using
 the "**" syntax:

>>> def process_data(a, b, c, d):
>>>    print(a, b, c, d)

>>> x = {'a': 1, 'b': 2}
>>> y = {'c': 3, 'd': 4}

>>> process_data(**x, **y)
1 2 3 4

>>> process_data(**x, c=23, d=42)
1 2 23 42

### def print_genius(*names):
A parameter prefixed by one * can capture any number of positional arguments into a tuple.


### def top_genius(**names):
A parameter prefixed by two * can capture any number of keyword arguments into a dict.


### def func(*args, **kwargs):
By convention, we define a function like the following if the number of its arguments cannot be determined

def func(*args, **kwargs):
    print(f"*args: {args}")
    print(f"kwargs: {kwargs}")

func(2, None, t1=1) 

*args: (2, None)
kwargs: {'t1': 1}


## Literal parameter (Python3.8)
def parse_bool_str(
    text: str,
    default: Literal["raise", False] = "raise"
) -> bool:


## Lists
### Delete all elements in a list
>>> lst = [1, 2, 3, 4, 5]
>>> del lst[:]
>>> lst
[]

### Access
    a = "Hello world!"
    backward = a[::-1]
    print(backward)
    !dlrow olleH

## Dict
### Sort dicts (lambda and itemgetter)
```python
>>> xs = {'a': 4, 'b': 3, 'c': 2, 'd': 1}
>>> sorted(xs.items(), key=lambda x: x[1])
[('d', 1), ('c', 2), ('b', 3), ('a', 4)]
```

```python
>>> xs = {'a': 4, 'b': 3, 'c': 2, 'd': 1}
>>> import operator
>>> sorted(xs.items(), key=operator.itemgetter(1))
[('d', 1), ('c', 2), ('b', 3), ('a', 4)]
```


## Collections.counter
    import collections
    c = collections.Counter('helloworld')

    c
    Counter({'l': 3, 'o': 2, 'e': 1, 'd': 1, 'h': 1, 'r': 1, 'w': 1})

    c.most_common(3)
    [('l', 3), ('o', 2), ('e', 1)]

```python
from collections import Counternum_lst = [1, 1, 2, 3, 4, 5, 3, 2, 3, 4, 2, 1, 2, 3]

cnt = Counter(num_lst)
print(dict(cnt))# first 2 most occurrence
print(dict(cnt.most_common(2)))str_lst = ['blue', 'red', 'green', 'blue', 'red', 'red', 'green']
print(dict(Counter(str_lst)))

# 1: 3, 2: 4, 3: 4, 4: 2, 5: 1}
# {2: 4, 3: 4}
# {'blue': 2, 'red': 3, 'green': 2}
```

## Permutations
### itertools.permutations() generates permutations 
### for an iterable. Time to brute-force those passwords ;-)

    import itertools
    for p in itertools.permutations('ABCD'):
    ...     print(p)

('A', 'B', 'C', 'D')
('A', 'B', 'D', 'C')
('A', 'C', 'B', 'D')
('A', 'C', 'D', 'B')
('A', 'D', 'B', 'C')
('A', 'D', 'C', 'B')
('B', 'A', 'C', 'D')
('B', 'A', 'D', 'C')
('B', 'C', 'A', 'D')
('B', 'C', 'D', 'A')
('B', 'D', 'A', 'C')
('B', 'D', 'C', 'A')
('C', 'A', 'B', 'D')
('C', 'A', 'D', 'B')
('C', 'B', 'A', 'D')
('C', 'B', 'D', 'A')
('C', 'D', 'A', 'B')
('C', 'D', 'B', 'A')
('D', 'A', 'B', 'C')
('D', 'A', 'C', 'B')
('D', 'B', 'A', 'C')
('D', 'B', 'C', 'A')
('D', 'C', 'A', 'B')
('D', 'C', 'B', 'A')

## f-string

### multiline
https://medium.com/bitgrit-data-science-publication/python-f-strings-tricks-you-should-know-7ce094a25d43

```python
company_name = "Tesla"
employee_count = 100000
mission = "To accelerate the world's transition to sustainable energy"

print(f"""
Company: {company_name}
# of employees: {employee_count:,}
Mission: {mission}
""")
```

```yaml
Company: Tesla
# of employees: 100,000
Mission: To accelerate the world's transition to sustainable energy
```



### Repr & str
https://medium.com/bitgrit-data-science-publication/python-f-strings-tricks-you-should-know-7ce094a25d43

```python
from dataclasses import dataclass

@dataclass
class Person:
    name : str
    age : int

    def __str__(self) -> str:
        return f"{self.name} is {self.age} years old"

Elon = Person("Elon Musk", 51)
print(f"{Elon}") # str
print(f"{Elon!r}") # repr
```


```yaml
Elon Musk is 51 years old
Person(name='Elon Musk', age=51)
```


### Nested f-strings
```python
number = 254.3463
print(f"{f'${number:.3f}':>10s}")
# '  $254.346'
```

```python
>>> a={"b":1}
>>> c="b"
>>> f"{a=}"
"a={'b': 1}"
>>> f"{a['b']}"
'1'
>>> f"{a['b']=}"
"a['b']=1"
>>> f"{a[f'{c}']}"
'1'
>>> f"{a[f'{c}']=}"
"a[f'{c}']=1"
```

### f-string Formating
```python
text = "hello world"

# Center text:
print(f"{text:^15}")
# '  hello world  '

number = 1234567890
# Set separator
print(f"{number:,}")
# 1,234,567,890

number = 123
# Add leading zeros
print(f"{number:08}")
# 00000123
```

### f-string floats
```python
x = 10
print(f"{x = :.3f}")
# x = 10.000
```

### f-tring date representation
```python
import datetime
today = datetime.datetime.today()
print(f"{today:%Y-%m-%d}")
# 2022-03-11
print(f"{today:%Y}")
# 2022
```

https://medium.com/bitgrit-data-science-publication/python-f-strings-tricks-you-should-know-7ce094a25d43


```python
import datetime

today = datetime.datetime.utcnow()
print(f"datetime : {today}\n")

print(f"date time: {today:%m/%d/%Y %H:%M:%S}")
print(f"date: {today:%m/%d/%Y}")
print(f"time: {today:%H:%M:%S.%f}") 
print(f"time: {today:%H:%M:%S %p}") 
print(f"time: {today:%H:%M}")
```

```yaml
datetime : 2022-09-13 05:44:17.546036

date time: 09/13/2022 05:44:17
date: 09/13/2022
time: 05:44:17.546036
time: 05:44:17 AM
time: 05:44
```


### f-string for debuging in python 3.8
print(f'{pi=}')          # pi=3


## objects
### Redefine ==
https://medium.com/techtofreedom/10-remarkable-python-oop-tips-that-will-optimize-your-code-significantly-a47e4103b44d

```python
class Point:
    def __init__(self,x,y):
        self.x = x
        self.y = y

A=Point(2,3)
B=Point(2,3)
print(A==B)
# False
```

```python
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

A=Point(2,3)
B=Point(2,3)
print(A==B)
# True
```

### @dataclass decorator >3.7
https://medium.com/techtofreedom/10-remarkable-python-oop-tips-that-will-optimize-your-code-significantly-a47e4103b44d

from dataclasses import dataclass

```python
@dataclass
class Point:
    x:int
    y:int

A=Point(2,3)
B=Point(2,3)
print(A==B)
# True
```

### Abstract class
https://medium.com/techtofreedom/10-remarkable-python-oop-tips-that-will-optimize-your-code-significantly-a47e4103b44d

```python
from abc import ABC, abstractmethod

class Animal(ABC):
    @abstractmethod
    def move(self):
        print('Animal moves')

class Cat(Animal):
    def move(self):
        super().move()
        print('Cat moves')

c = Cat()
c.move()

# Animal moves
# Cat moves
```

### Separate Class-Level and Instance-Level Attributes
https://medium.com/techtofreedom/10-remarkable-python-oop-tips-that-will-optimize-your-code-significantly-a47e4103b44d

```python
class MyClass(object):
     class_attr = 0
     def __init__(self, instance_attr):
         self.instance_attr = instance_attr
        
MyClass.class_attr
# 0

MyClass.instace_attr
# AttributeError: type object 'MyClass' has no attribute 'instace_attr'

my_instance = MyClass(1)

my_instance.instance_attr
# 1

my_instance.class_attr
# 0
```

### Separate Public, Protected and Private Attributes
https://medium.com/techtofreedom/10-remarkable-python-oop-tips-that-will-optimize-your-code-significantly-a47e4103b44d

This is just a convention. We can still use it as a public member.

```python
class Student:
   def __init__(self, name, age, grade):
   self.name = name # public
   self._age = age # protected
   self.__grade = grade # private
```

### Use @property Decorator To Control Attributes Precisely
https://medium.com/techtofreedom/10-remarkable-python-oop-tips-that-will-optimize-your-code-significantly-a47e4103b44d

```python
class Student:
    def __init__(self):
        self._score = 0

    def set_score(self, s):
        if 0 <= s <= 100:
            self._score = s
        else:
            raise ValueError('The score must be between 0 ~ 100!')
Yang = Student()
Yang.set_score(100)
print(Yang._score)
# 100
```

Much bether ...

```python
class Student:
    def __init__(self):
        self._score = 0

    @property
    def score(self):
        return self._score

    @score.setter
    def score(self, s):
        if 0 <= s <= 100:
            self._score = s
        else:
            raise ValueError('The score must be between 0 ~ 100!')

    @score.deleter
    def score(self):
        del self._score

Yang = Student()

Yang.score=99
print(Yang.score)
# 99

Yang.score = 999
# ValueError: The score must be between 0 ~ 100!
```

### Use Static Methods in Classes
https://medium.com/techtofreedom/10-remarkable-python-oop-tips-that-will-optimize-your-code-significantly-a47e4103b44d

```python
class Student:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name
        self.nickname = None

    def set_nickname(self, name):
        self.nickname = name

    @classmethod
    def get_from_string(cls, name_string: str):
        first_name, last_name = name_string.split()
        return Student(first_name, last_name)

    @staticmethod
    def suitable_age(age):
        return 6 <= age <= 30


print(Student.suitable_age(99)) # False
print(Student.suitable_age(27)) # True
print(Student('yang', 'zhou').suitable_age(27)) # True
```

We can see that the static method is defined inside the class, but it doesn’t have access to the instance data or the class data. It can be called on the class itself or on an instance of the class.

Some common uses of static methods include utility functions that perform tasks such as formatting data or validating input, and methods that provide a logical grouping of related functions, but do not need to modify the state of the instance or the class.

Therefore, a good OOP practice is:

    Define a function as a static method within a class if this function’s logic is closely related to the class.


### Separate __new__ and __init__: Two Different Python Constructors
https://medium.com/techtofreedom/10-remarkable-python-oop-tips-that-will-optimize-your-code-significantly-a47e4103b44d

```python
class Singleton_Genius(object):
    __instance = None

    def __new__(cls, *args, **kwargs):
        if not Singleton_Genius.__instance:
            Singleton_Genius.__instance = object.__new__(cls)
        return Singleton_Genius.__instance

    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

s1 = Singleton_Genius("Yang", "Zhou")
s2 = Singleton_Genius("Elon", "Musk")

print(s1)
# <__main__.Singleton_Genius object at 0x7fec946b1760>
print(s2)
# <__main__.Singleton_Genius object at 0x7fec946b1760>
print(s1 == s2)
# True
print(s1.last_name)
# Musk
print(s2.last_name)
# Musk
```


### Use __slots__ for Better Attributes Control
https://medium.com/techtofreedom/10-remarkable-python-oop-tips-that-will-optimize-your-code-significantly-a47e4103b44d

```python
class Author:
    def __init__(self,name):
        self.name = name

me = Author('Yang')

me.age=29
print(me.age)
# 29
```

OMG!!

```python
class Author:
    __slots__ = ['name','hobby']
    def __init__(self,name):
        self.name = name

me = Author('Yang')

me.hobby='writing'

me.age=29
# AttributeError: 'Author' object has no attribute 'age'
```


### @classmethod vs @staticmethod vs "plain" methods
```
class MyClass:
    def method(self):
        """
        Instance methods need a class instance and
        can access the instance through `self`.
        """
        return 'instance method called', self

    @classmethod
    def classmethod(cls):
        """
        Class methods don't need a class instance.
        They can't access the instance (self) but
        they have access to the class itself via `cls`.
        """
        return 'class method called', cls

    @staticmethod
    def staticmethod():
        """
        Static methods don't have access to `cls` or `self`.
        They work like regular functions but belong to
        the class's namespace.
        """
        return 'static method called'

# All methods types can be
# called on a class instance:
>>> obj = MyClass()
>>> obj.method()
('instance method called', <MyClass instance at 0x1019381b8>)
>>> obj.classmethod()
('class method called', <class MyClass at 0x101a2f4c8>)
>>> obj.staticmethod()
'static method called'

# Calling instance methods fails
# if we only have the class object:
>>> MyClass.classmethod()
('class method called', <class MyClass at 0x101a2f4c8>)
>>> MyClass.staticmethod()
'static method called'
>>> MyClass.method()
TypeError: 
    "unbound method method() must be called with MyClass "
    "instance as first argument (got nothing instead)"
```

## copy Vs copy.copy() Vs copy.deepcopy()
```
a = [[0,1],[2,3]]
b = a

b[1][1] = 100

print(a,b) 
# [[0, 1], [2, 100]] [[0, 1], [2, 100]]
print(id(a)==id(b))
# True
```

```
import copy

a = [[0,1],[2,3]]
b = copy.copy(a)

print(id(a)==id(b))
# False

b[1] = 100
print(a,b)
# [[0, 1], [2, 3]] [[0, 1], 100]

b[0][0] = -999
print(a,b)
# [[-999, 1], [2, 3]] [[-999, 1], 100]
print(id(a[0]) == id(b[0]))
# True
```

## Input
```
# for two integer values.
x, y = map(int, input().split())

# taking input into a list.
arr = list(map(int, input().split()))
```

## If
### switchcase
Spaghetti code?
```python
def dispatch_if(operator, x, y):
    if operator == 'add':
        return x + y
    elif operator == 'sub':
        return x - y
    elif operator == 'mul':
        return x * y
    elif operator == 'div':
        return x / y
    else:
        return None
```
```python
def dispatch_dict(operator, x, y):
    return {
        'add': lambda: x + y,
        'sub': lambda: x - y,
        'mul': lambda: x * y,
        'div': lambda: x / y,
    }.get(operator, lambda: None)()
```


## Loops
### Enumerations
```
arr = ["a", "b", "c", "d", "e", "f"]
for index, val in enumerate(arr):
  print(f"{index} - {val}")
```

## Maths
### gcd (mcd)
```
def gcd(a, b):
  while(b): a, b = b, a % b
  return a
```

### lcd (mcm
```
from functools import reduce
from math import gcd

def lcm(numbers):
  return reduce((lambda x, y: int(x * y / gcd(x, y))), numbers)
  
lcm([12, 7]) # 84
lcm([1, 3, 4, 5]) # 60
```

## Zip
### range()
```
list(zip(range(3), 'abc'))
# [(0, 'a'), (1, 'b'), (2, 'c')]

names = ['Alice', 'Bob', 'Charlie']
ages = [24, 50, 18]
```

### Enumerate()
```
names = ['Alice', 'Bob', 'Charlie']

for i, name in enumerate(zip(names)):
    print(i, name)
# 0 Alice 24
# 1 Bob 50
# 2 Charlie 18

```

```python
lst = ['a', 'b', 'c', 'd']

for index, value in enumerate(lst):
    print(f"{index}, {value}")

for index, value in enumerate(lst, start=10):
    print(f"{index}, {value}")

0, a
1, b
2, c
3, d

10, a
11, b
12, c
13, d
```



### matrix
```
matrix = [[1, 2, 3], [1, 2, 3]]
matrix_T = [list(i) for i in zip(*matrix)]
print(f"matrix_T: {matrix_T}")
# matrix_T: [[1, 1], [2, 2], [3, 3]]
```

## regex
### patter maching
```python
import re
pattern=r"abc$"
s1="abcPYTHONacbcabc"
s2=re.sub(pattern,"",s)1
print (s2)
#Output:abcPYTHONacbc

# pattern=r”abc$” → Checks whether the string ends with “abc”
# $ →indicates the end of the string.
```


## Pandas
    import pandas as pd
    import numpy as np
    df = pd.read_csv("weatherHistory.csv")

    df.head(5)

        NA Label
        0   0    TN
        1   1    GA
        2   0    TN
        3   1    MN
        4   0    CA


## ipython3 notebook
cd ~/treball/dev/python/jupyter

ipython3 notebook


## @classmethod vs @staticmethod vs "plain" methods
```
class MyClass:
    def method(self):
        """
        Instance methods need a class instance and
        can access the instance through `self`.
        """
        return 'instance method called', self

    @classmethod
    def classmethod(cls):
        """
        Class methods don't need a class instance.
        They can't access the instance (self) but
        they have access to the class itself via `cls`.
        """
        return 'class method called', cls

    @staticmethod
    def staticmethod():
        """
        Static methods don't have access to `cls` or `self`.
        They work like regular functions but belong to
        the class's namespace.
        """
        return 'static method called'

# All methods types can be
# called on a class instance:
>>> obj = MyClass()
>>> obj.method()
('instance method called', <MyClass instance at 0x1019381b8>)
>>> obj.classmethod()
('class method called', <class MyClass at 0x101a2f4c8>)
>>> obj.staticmethod()
'static method called'

# Calling instance methods fails
# if we only have the class object:
>>> MyClass.classmethod()
('class method called', <class MyClass at 0x101a2f4c8>)
>>> MyClass.staticmethod()
'static method called'
>>> MyClass.method()
TypeError: 
    "unbound method method() must be called with MyClass "
    "instance as first argument (got nothing instead)"
```

## Lampda expressions
How to Use Lambda Expressions in Python

Instead of writing a separate function to calculate the cube of a number, we can use a lambda expression in its place. Here's how you'd do that:

```python
fin_list = list(map(lambda x:x**3, org_list))
print(fin_list) # [1, 8, 27, 64, 125]
```

Much cleaner, wouldn't you agree? 


## Infinity values in Python

```python
a = float("inf")
b = float("-inf")
```

We can define infinity values in Python (above). Positive infinity is larger than all numbers, while negative infinity is smaller than all numbers.


## Using ‘pprint’ to print stuff nicely

We can use pprint to nicely print out complicated data structures without having to write any loops.

```python
from pprint import pprint
d = {"A":{"apple":1, "orange":2, "pear":3}, "B":{"apple":4, "orange":5, "pear":6}, "C":{"apple":7, "orange":8, "pear":9}}
pprint(d)
```


## Unprinting stuff in Python

CURSOR_UP = "\033[1A"
CLEAR = "\x1b[2K"

The "\033[1A" escape character moves our cursor up by 1 line, and the "\x1b[2K" character clears the entire current line. If we print them together, we can essentially ‘unprint’ an entire line in our terminal

```python
print("apple")
print("orange")
print("pear")print((CURSOR_UP + CLEAR)*2, end="") # UNPRINTS 2 LINES
print("pineapple")
```

Note — remember to use the end="" option in the print function.


## Python has a backspace character

print("abc" + "\b" + "d")# abd

The backspace character \b backspaces the stuff that we print.


## The walrus operator

The walrus operator := was introduced in Python 3.8. It’s basically the same as the assignment operator =, but it also returns the value itself. We can thus save one line of code when writing conditional statements.

```python
x = 5
if x > 3:
    print("x is more than 3")
```

This is the same as:

```python
if (x := 5) > 3:
    print("x is more than 3")
```


# Python-modules

## json.tool
python -m json.tool --sort-keys --json-lines < data.jsonl


## Logging
https://towardsdatascience.com/why-and-how-to-set-up-logging-for-python-projects-bcdd4a374c7a

```python
# src/main.py
import logging
import sys

from data_processing.processor import process_data
from model_training.trainer import train

# instantiate logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# define handler and formatter
handler = logging.StreamHandler()
formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(name)s - %(message)s")

# add formatter to handler
handler.setFormatter(formatter)

# add handler to logger
logger.addHandler(handler)


if __name__ == "__main__":
    logger.info("Program started")
    process_data()
    train()
    logger.info("Program finished")
```


```python
#### src/model_training/trainer.py ####
import logging

logger = logging.getLogger(__name__)

def train():
"""Dummy training function"""
logger.info("Training model")
# model training code here
logger.info("Model training complete")


#### src/data_processing/processor.py ####
import logging

logger = logging.getLogger(__name__)

def process_data():
"""Dummy data processing function"""
logger.info("Pre-processing data")
# data preprocessing code here...
logger.info("Data pre-processing complete")
```

```python
# src/main.py
import logging
import logging.config
import os
from datetime import datetime

from data_processing.processor import process_data
from dotenv import find_dotenv, load_dotenv
from model_training.trainer import train

# find .env file in parent directory
env_file = find_dotenv()
load_dotenv()

CONFIG_DIR = "./config"
LOG_DIR = "./logs"


def setup_logging():
    """Load logging configuration"""
    log_configs = {"dev": "logging.dev.ini", "prod": "logging.prod.ini"}
    config = log_configs.get(os.environ["ENV"], "logging.dev.ini")
    config_path = "/".join([CONFIG_DIR, config])

    timestamp = datetime.now().strftime("%Y%m%d-%H:%M:%S")

    logging.config.fileConfig(
        config_path,
        disable_existing_loggers=False,
        defaults={"logfilename": f"{LOG_DIR}/{timestamp}.log"},
    )


if __name__ == "__main__":

    setup_logging()
    logger = logging.getLogger(__name__)

    logger.info("Program started")
    process_data()
    train()
    logger.info("Program finished")
```

```ini
[loggers]
keys=root,file

[handlers]
keys=console,file

[formatters]
keys=console,file

[logger_root]
level=DEBUG
handlers=console,file

[logger_file]
level=DEBUG
handlers=file
qualname=file

[handler_console]
class=StreamHandler
level=DEBUG
formatter=console
args=(sys.stdout,)

[handler_file]
class=FileHandler
level=DEBUG
formatter=file
args=('%(logfilename)s','w',)

[formatter_console]
format=%(asctime)s - %(levelname)s - %(name)s - %(message)s

[formatter_file]
format=%(asctime)s - %(levelname)s - %(name)s - %(funcName)s:%(lineno)d - %(message)s
```


## Loguru: Logs
https://github.com/Delgan/loguru


## pycg and pyvis: Automated call grafs
https://www.freecodecamp.org/news/how-to-automate-call-graph-creation/




## PymMouse: Automate Mouse Actions

pip install PyMouse

```python
from pymouse import PyMousepy = PyMouse()# Get Position of Mouse Pointer
x, y = py.position()
print(x, y)# Move Mouse to a new Position
py.move(x, y)# Click left Mouse Button
py.click(x, y, 1)# Click right Mouse Button
py.click(x, y, 2)# Click middle Mouse Button
py.click(x, y, 3)# Press Mouse button 
py.press(x, y)# Release Mouse button
py.release(x, y)
```

## argparse
* https://python.plainenglish.io/mastering-command-line-arguments-in-python-a-comprehensive-guide-with-argparse-3da3b8984dd9
* https://itnext.io/how-to-build-command-line-programs-in-python-af6396ad40a6


```python
import argparse 
parser = argparse.ArgumentParser(prog='myprogram', description='Process some files') 

# Required arguments 
parser.add_argument('input', help='The input file') 
parser.add_argument('output', help='The output file') 

# Optional arguments 
parser.add_argument('-v', ' - verbose', action='store_true', help='Enable verbose output')
parser.add_argument('-o', ' - overwrite', action='store_true', help='Overwrite the output file if it exists')
parser.add_argument('-n', ' - num-records', type=int, default=10, help='The number of records to process') 

# Mutually exclusive arguments 
group = parser.add_mutually_exclusive_group() 
group.add_argument('-u', ' - uppercase', action='store_true', help='Convert the output to uppercase') 
group.add_argument('-l', ' - lowercase', action='store_true', help='Convert the output to lowercase') 

# Arguments with choices 
parser.add_argument('-c', ' - compression', choices=['gzip', 'bzip2', 'lzma'], help='The compression algorithm to use') 

# Positional arguments with variable number of inputs 
parser.add_argument(' - exclude', nargs='+', help='Exclude the specified files from processing') 

# Parse the command line arguments 
args = parser.parse_args()
```


## pyperf: Performance analysis (v > 3,10)
python -m pyperf timeit -s 'k = "foo"; v = "bar"' -- '"%s = %r" % (k, v)'

## Faker: Faker is a Python package that generates fake personal data for you
https://github.com/joke2k/faker

## Instant http server
python -m http.server

python -m http.server --directory <desired-directory>

python -m http.server 8080 --directory /tmp/ --bind 127.0.0.1


## argparse
* https://medium.com/@andresberejnoi/supercharge-your-python-scripts-with-command-line-interfaces-andres-berejnoi-f906897b915f
* https://github.com/andresberejnoi/PublicNotebooks/tree/master/Command%20Line%20Menu%20Morse%20Code%20Coverter

```python
import argparse
import nato_alphabet as nt
def cli():
    parser = argparse.ArgumentParser(description='Morse Code Tool')
    parser.add_argument('-s', '--source', type=str, default=None,
                        help="File containing the text to translate.")
    parser.add_argument('-t', '--text', type=str, default='',
                        help="""Enter text to translate directly. 
                                If `--source` is provided, then 
                                this option (`--text`) is
                                ignored.""")
    parser.add_argument('-m', '--mode', type=str, 
                        choices=['morse','telephony'], 
                        default='morse',
                        help="""Choose mode of operation: morse or telephony.""")
    parser.add_argument('-d', 
                        '--direction', 
                        type=str, 
                        choices=['from_text', 'to_text'], 
                        default='from_text',
                        help="""Select the direction of
                             the convertion.
                             If the source text needs 
                             to be converted to morse code, 
                             then choose `from_text` (this 
                             is the default) and if the 
                             source text is already morse code
                             and you want to decrypt it, 
                             choose `to_text`.""")
    parser.add_argument('-o','--output', type=str, default=None,
                        help="""File where to save the output. 
                             If this flag is not provided, 
                             the output will be printed 
                             to the terminal.""")
    args = parser.parse_args()
    return args
def main(args):
    #-- Get the source text to translate
    text = ""
    if args.source:
        with open(args.source, 'r') as file_handler:
            text = file_handler.read()
    else:
        text = args.text    text = text.lower()    #-- get mode and direction of the conversion, and output file
    mode      = args.mode
    direction = args.direction
    out_file = args.output    #-- Convert text
    output_str = ''
    
    if direction == 'from_text':
        if mode == 'morse':
            for letter in text:
                output_str += nt.MORSE_CODE_MAPPER.get(letter, '*') + '^'
        else:
            for letter in text:
                output_str += nt.TELEPHONY_MAPPER.get(letter, '*') + '^'    elif direction == 'to_text':
        text = text.strip('^').strip()
        if mode == 'morse':
            print(text.split('^'))
            for letter in text.split('^'):
                output_str += nt.REVERSE_CODE_MAPPER.get(letter, '*')
    
        else:
            for letter in text:
                output_str += nt.REVERSE_TELEPHONY_MAPPER.get(letter, '*')    #-- clean up the result a little bit
    output_str = output_str.strip('^').strip(' ')    #-- write result to file or to the terminal
    if out_file:
        with open(out_file, 'w') as file_handler:
            file_handler.write(output_str)
    else:
        print(output_str)if __name__ == '__main__':
    args = cli()
    main(args)
```


# Systemd services
## List services
systemctl list-unit-files

## List enabled services
systemctl list-unit-files | grep enabled




# Systemd timers
## List timers
systemctl list-timers --all


# Netbox
## Netbox dev dict
    {
        "id": 96,
        "name": "TRACAM3",
        "display_name": "TRACAM3",
        "device_type": {
            "id": 14,
            "url": "https://netbox.<server>.com/api/dcim/device-types/14/",
            "manufacturer": {
            "id": 12,
            "url": "https://netbox.<server>.com/api/dcim/manufacturers/12/",
            "name": "Axis",
            "slug": "axis"
            },
            "model": "P3224-V MK II",
            "slug": "p3224-v",
            "display_name": "Axis P3224-V MK II"
        },
        "device_role": {
            "id": 11,
            "url": "https://netbox.<server>.com/api/dcim/device-roles/11/",
            "name": "Camera",
            "slug": "camera"
        },
        "tenant": {
            "id": 2,
            "url": "https://netbox.<server>.com/api/tenancy/tenants/2/",
            "name": "<tenant1>",
            "slug": "fgc"
        },
        "platform": null,
        "serial": "ACCC8EB8B8C5",
        "asset_tag": "FGC.70",
        "site": {
            "id": 13,
            "url": "https://netbox.<server>.com/api/dcim/sites/13/",
            "name": "<tenant2>",
            "slug": "<tenant2>"
        },
        "rack": null,
        "position": null,
        "face": null,
        "parent_device": null,
        "status": {
            "value": "planned",
            "label": "Planned",
            "id": 2
        },
        "primary_ip": null,
        "primary_ip4": null,
        "primary_ip6": null,
        "cluster": null,
        "virtual_chassis": null,
        "vc_position": null,
        "vc_priority": null,
        "comments": "",
        "local_context_data": null,
        "tags": [],
        "custom_fields": {
            "last_ping": null,
            "Operating System": "8.40.2.2",
            "Project": null,
            "Purchase Date": "2018-10-18",
            "Warranty Expiration": "2021-10-18"
        },
        "config_context": {},
        "created": "2019-10-11",
        "last_updated": "2019-10-11T09:37:15.752732Z"
    }


# mariadb
## update
update issue set closed=0 where id=6;

## update date
UPDATE calldata SET date = '2014-01-01' WHERE DATE(date) = '2014-01-01'

## update datetime adding some time
UPDATE monitoring SET time = DATE_ADD(time, INTERVAL 2 MONTH);

## update datetime directly
UPDATE issue SET time ='2020-07-14 00:00:01' where id=1;


## insert
### insert now()
insert into post values (1,"body1",now(),1);

insert into issue (time,reason,device) values ("2020-06-12",300,49);


### insert utc time
insert into issue (time, reason, device) value (utc_timestamp(),600,2);

### insert with columns
insert into issue (time,reason,device) values 
UPDATE calldata SET date = '2014-01-01' WHERE DATE(date) = '2014-01-01'



### insert a dict in a text field
insert into issue (time,reason,device,closetime,solution,closed,json) value ('2020-07-14 02:00:00',300,1,'2020-07-15 05:00:00','1000','1','{"id": 1, "name": "aaa aaa", "display_name": "aaa aaa", "device_role": "Camera", "site": "site ccc", "site_slug": "site-ccc2", "tenant": "<notDefined>"}');

## delete
delete from issue where id=12;

## show grants
```show grants;```
```
+----------------------------------------------------------------------------------------------------------------------+
| Grants for root@192.168.%                                                                                            |
+----------------------------------------------------------------------------------------------------------------------+
| GRANT ALL PRIVILEGES ON *.* TO `root`@`192.168.%` IDENTIFIED BY PASSWORD '*B21543CBDF2FA33B8E428BCE9F26780AB8EDC26B' |
+----------------------------------------------------------------------------------------------------------------------+
1 row in set (0.005 sec)
```

show grants for samse@'%';
+---------------------------------------------------------------------------------------------------------------+
| Grants for samse@%                                                                                            |
+---------------------------------------------------------------------------------------------------------------+
| GRANT ALL PRIVILEGES ON *.* TO 'samse'@'%' IDENTIFIED BY PASSWORD '****' |
+---------------------------------------------------------------------------------------------------------------+

## Show servername
show variables where Variable_name='hostname';

## Show variables
show variables where Variable_name like 'server_audit%';



# YUM (Redhat)
## List packages
### yum list installed http*
Llista els packets installats amb la cadena http*

### yum list-security --security
List only updatable installed packages



# VScode - VSCodium
## Find & Replace
### Regex for newline
\n\r

# Redmine
## External links
"Redmine web site":http://www.redmine.org

## Textile formating
https://www.redmine.org/projects/redmine/wiki/RedmineTextFormattingTextile

* [[sandbox:some page]] displays a link to the page named 'Some page' of the Sandbox wiki
* [[sandbox:]] displays a link to the Sandbox wiki main page

## Wiki formating link
https://www.redmine.org/help/en/wiki_syntax_detailed.html

## Wiki formating text color
https://www.redmine.org/issues/11056

%{color:red}red%

p{color:red}. this is red

%{background:lightgreen}Lightgreen

## Redmine and git
https://www.redmine.org/projects/redmine/wiki/RedmineSettings#Referencing-issues-in-commit-messages

Refs #22764.
This commit refs #1, #2 and fixes #3
This commit refs:#1, #2 and fixes #3
This commit Refs  #1, #2 and fixes #3
This commit REFS: #1, #2 and fixes #3


etckeeper:commit:etckeeper_sucs836_etc|99e9a5b0035875d5cfcd331ec5cc7c4e09e5afc9.



## Redmine repos
https://www.redmine.org/help/en/wiki_syntax_detailed.html

<project>:source:<repo>|<some/file> (link to a file of a specific repository, for projects with multiple repositories)

## Link to a commit
project-identifier:commit:hash  -> In case of main repor
project-identifier:<repo>|commit:hash -> Other repo

## Link to a repo line
etckeeper:source:etckeeper_sucs905_etc|httpd/conf.d/docker-registry.conf#L12

## wiki Includes
{{include(<projectName>:<wikiPage>)}}

## Textile collapse
https://www.redmine.org/projects/redmine/wiki/RedmineTextFormattingTextile#Macros

collapse

Inserts of collapsed block of text. Example:

```yaml
{{collapse(View details...)
This is a block of text that is collapsed by default.
It can be expanded by clicking a link.
}}
```

## Macros
https://www.redmine.org/issues/29489

```
##123                                       -- Issue #123: Enhance macro capabilities
{{issue(123)}}                              -- Issue #123: Enhance macro capabilities
{{issue(123, project=true)}}                -- Andromeda - Issue #123:Enhance macro capabilities
{{issue(123, tracker=false)}}               -- #123: Enhance macro capabilities
{{issue(123, subject=false, project=true)}} -- Andromeda - Issue #123
```

## Textile escape
<notextile>aaaaa</notextile>

# xclip
## Add file content to clipboard
cat ~/.ssh/joangvillaraco_<server>.pub | xclip -sel clip

# cat
## cat > lxd-init.yaml <<EOF
cat > lxd-init.yaml <<EOF
lin1
lin2
EOF

# netstat
## netstat -putona
netstat -putona
```
p Muestra las conexiones para el protocolo especificado que puede ser TCP o UDP
u Lista todos los puertos UDP
t Lista todos los puertos TCP
o Muestra los timers
n Muestra el numero de puerto
a Visualiza todas las conexiones activas del sistema
```
## netstat -capullo
netstat -capullo



# date
## | tee ~/treball/<server>/logs/tinkiservice_`date +"%Y%m%d%H%M%S"`.log
Reenvia la sortida a stdout i al fitxer escollit evaluant la sortida del date





## ssh
### ssh remote execute
ssh <user>@<host> <command>

### Jump host
cat .ssh/config
```yaml
...
Host ocloud02
    Hostname A.B.C.D
    Port 22
    User ubuntu
    IdentityFile ~/.ssh/oracle-k3s-ssh-key-2022-11-22.key

Host ocloud03-worker1
    Hostname 10.0.1.206
    Port 22
    User ubuntu
    IdentityFile ~/.ssh/oracle-k3s-ssh-key-2022-11-22.key
    ProxyJump ocloud02

Host ocloud04-worker2
    Hostname 10.0.1.224
    Port 22
    User ubuntu
    IdentityFile ~/.ssh/oracle-k3s-ssh-key-2022-11-22.key
    ProxyJump ocloud02
```

Notes:
* Private (and public) keys are just in ssh client server


ssh ocloud03-worker1



# postgres
## psql -U tinki -h localhost

## \l
List databases

## psql -U tinki -h localhost -l
List databases and exits

## \du
List users


## \dt
List tables in database

## select table_name,column_name,data_type from information_schema.columns where table_name='color';
Describe table color

## psql -U tinki -h localhost -c \"select ...\"
psql -U table1 -h localhost -c "select table_name,column_name,data_type from information_schema.columns where table_name='color';"

psql -U table1 -h localhost -c "select count(*) from project;"

## No pager on psql -U tinki -h localhost -c \"select ...\"
Add \pset pager off to ~/.psqlrc

or

psql -P pager=off -U tinki -h localhost -c \"select ...\"


## docker exec -ti aaaservice_postgres_1 psql -U tinki




## create database test;


## create user test_user with encrypted password 'pepepe';


## grant all privileges on database test to test_user;



## .pgpass
```
~/.pgpass
hostname:port:database:username:password
```

  # pytest
## pytest -s
Enable show prints

## pytest 
Run all test

## pytest test/\<someTest>
Single test

## pytest test/\<someTest> -k <matchingTest>
Execute <matchingTest> in file test/\<someTest>

## pytest -s test/\<someTest> -k <matchingTest> --capture=no
Execute <matchingTest> in file test/\<someTest> printing out error as they happen (no buffering for the end)

## pytest with tee 
pytest -s tests/test_project_tmp.py 2>&1 | tee ~/treball/<server>/aux/test_project_tmp_`date +"%Y%m%d%H%M%S"`.log

## pytest fail
@pytest.mark.xfail(raises=IndexError)

@pytest.mark.xfail(reason="Forced fail")


## mock.patch
import os
import mock

## pytest logglevel=debug
pytest -s tests/test_project_publicproject.py -k test_publicproject_archived -log-cli-level=DEBUG


@mock.patch("os.listdir", mock.MagicMock(return_value="test1"))
def test1():
    assert "test1" == os.listdir()


 @mock.patch("os.listdir")
 def test2(mock_listdir):
     mock_listdir.return_value = "test2"
     assert "test2" == os.listdir()


 @mock.patch("os.listdir")
 class Test():

     def not_decorated_and_not_tested(self):
         assert False

     def test3(self, mock_listdir):
         mock_listdir.return_value = "test3"
         assert "test3" == os.listdir()

## Dummy test
```python
# This funtion has been intencionally left commented in order easy test
#   endpoint /publicproject/dummy_test
# Please refer to publicproject/views.py/dummy_test funtion for more info.

def test_dummy_test(client):
    """Litle test to check dummy_test publicproject blueprint config"""
    print(f"Calling dummy test ...")
    response = client.get("/publicprojects/dummy_test/")

    print(f"response: {response}")
    print(f"response.json: {response.json}, "
          f"type(response.json): {type(response.json)}", file=sys.stderr)
    d_response = response.json

    # assert False
    assert 'success' in d_response and d_response['success']
```

# openssl
## echo | openssl s_client -servername api.loxonecloud.com -connect api.loxonecloud.com:443 2>/dev/null | openssl x509 -noout -dates
start and enddate
```
notBefore=Jan 25 08:31:34 2021 GMT
notAfter=Apr 25 08:31:34 2021 GMT
```

# vi
## Comentar més d'una línia
:10,20s/^/#/

## Copy to external clipboard
Requeriment: package vim-gtk

Select with mouse + "+y -> copy to clipboard

To paste: "+p

# tar
## tar create (1cpu)
tar -cvzf db2000778_base.tgz db2000778_base.qcow2

## tar create (all cpus)
tar -c --use-compress-program=pigz -f db2000778_base2.tgz db2000778_base.qcow2

(requirement package pigz)

# k8s - Kubernetes
## Service concepts
https://medium.com/devops-mojo/kubernetes-service-types-overview-introduction-to-k8s-service-types-what-are-types-of-kubernetes-services-ea6db72c3f8c

### ClusterIP
* ClusterIP is the default and most common service type.
* Kubernetes will assign a cluster-internal IP address to ClusterIP service. This makes the service only reachable within the cluster.
* You cannot make requests to service (pods) from outside the cluster.
* You can optionally set cluster IP in the service definition file.

Use Cases
* Inter service communication within the cluster. For example, communication between the front-end and back-end components of your app.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-backend-service
spec:
  type: ClusterIP # Optional field (default)
  clusterIP: 10.10.0.1 # within service cluster ip range
  ports:
  - name: http
    protocol: TCP
    port: 80
    targetPort: 8080
```

### NodePort
* NodePort service is an extension of ClusterIP service. A ClusterIP Service, to which the NodePort Service routes, is automatically created.
* It exposes the service outside of the cluster by adding a cluster-wide port on top of ClusterIP.
* NodePort exposes the service on each Node’s IP at a static port (the NodePort). Each node proxies that port into your Service. So, external traffic has access to fixed port on each Node. It means any request to your cluster on that port gets forwarded to the service.
* You can contact the NodePort Service, from outside the cluster, by requesting <NodeIP>:<NodePort>.
* Node port must be in the range of 30000–32767. Manually allocating a port to the service is optional. If it is undefined, Kubernetes will automatically assign one.
* If you are going to choose node port explicitly, ensure that the port was not already used by another service.
Use Cases
* When you want to enable external connectivity to your service.
* Using a NodePort gives you the freedom to set up your own load balancing solution, to configure environments that are not fully supported by Kubernetes, or even to expose one or more nodes’ IPs directly.
* Prefer to place a load balancer above your nodes to avoid node failure.


```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-frontend-service
spec:
  type: NodePort
  selector:
    app: web
  ports:
  - name: http
    protocol: TCP
    port: 80
    targetPort: 8080
    nodePort: 30000 # 30000-32767, Optional field
```

###  LoadBalancer
* LoadBalancer service is an extension of NodePort service. NodePort and ClusterIP Services, to which the external load balancer routes, are automatically created.
* It integrates NodePort with cloud-based load balancers.
* It exposes the Service externally using a cloud provider’s load balancer.
* Each cloud provider (AWS, Azure, GCP, etc) has its own native load balancer implementation. The cloud provider will create a load balancer, which then automatically routes requests to your Kubernetes Service.
* Traffic from the external load balancer is directed at the backend Pods. The cloud provider decides how it is load balanced.
* The actual creation of the load balancer happens asynchronously.
* Every time you want to expose a service to the outside world, you have to create a new LoadBalancer and get an IP address.


Use Cases
* When you are using a cloud provider to host your Kubernetes cluster.
* This type of service is typically heavily dependent on the cloud provider.


```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-frontend-service
spec:
  type: LoadBalancer
  clusterIP: 10.0.171.123
  loadBalancerIP: 123.123.123.123
  selector:
    app: web
  ports:
  - name: http
    protocol: TCP
    port: 80
    targetPort: 8080
```

### ExternalName
* Services of type ExternalName map a Service to a DNS name, not to a typical selector such as my-service.
* You specify these Services with the `spec.externalName` parameter.
* It maps the Service to the contents of the externalName field (e.g. foo.bar.example.com), by returning a CNAME record with its value.
* No proxying of any kind is established.

Use Cases
* This is commonly used to create a service within Kubernetes to represent an external datastore like a database that runs externally to Kubernetes.
* You can use that ExternalName service (as a local service) when Pods from one namespace to talk to a service in another namespace.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  type: ExternalName
```



## Cheatsheets
* https://kubernetes.io/docs/reference/kubectl/cheatsheet/


## Common pod operations
kubectl run ping --image=alpine --restart=Never -- ping 8.8.8.8 
  pod/ping created


$ kubectl logs ping --tail=5 --timestamps
```yaml
2023-02-28T15:12:40.258188552+02:00 64 bytes from 8.8.8.8: seq=669 ttl=54 time=49.607 ms
2023-02-28T15:12:41.258237567+02:00 64 bytes from 8.8.8.8: seq=670 ttl=54 time=49.517 ms
2023-02-28T15:12:42.258456090+02:00 64 bytes from 8.8.8.8: seq=671 ttl=54 time=49.546 ms
2023-02-28T15:12:43.258481029+02:00 64 bytes from 8.8.8.8: seq=672 ttl=54 time=49.481 ms
2023-02-28T15:12:44.258586900+02:00 64 bytes from 8.8.8.8: seq=673 ttl=54 time=49.467 ms
```

$ kubectl attach ping


kubectl exec ping -- /bin/sh


$ kubectl delete pod ping
  pod "ping" deleted



## K8s cluster test

## Kubectl bash autocompletion
https://medium.com/nerd-for-tech/enable-auto-completion-for-kubectl-in-a-linux-bash-shell-16420d42e3ec

8. kubectl completion bash | sudo tee /etc/bash_completion.d/kubectl > /dev/null
9. Reload your bash shell. Type kubectl -followed by pressing tab twice to see the available options and verify auto-complete is working:
10. Set an alias for kubectl as k -> echo 'alias k=kubectl' >>~/.bashrc
11. Enable the alias for auto-completion -> echo 'complete -o default -F __start_kubectl k' >>~/.bashrc
12. Reload you bash shell.


### kubectl get all --show-labels
### kubectl get --raw='/readyz?verbose' ~ componentstatus
### kubectl get nodes -o wide
### crictl ps
### ip ad
### kubectl get pods --all-namespaces
### kubectl cluster-info

## k8s destroy node
kubeadm reset

## K8s commands

### kubectl config current-context
https://www.redhat.com/sysadmin/sudo-kubernetes

### kubectl get node --as developer
https://www.redhat.com/sysadmin/sudo-kubernetes


### kubectl get node --as admin
https://www.redhat.com/sysadmin/sudo-kubernetes


### kubectl explain deployment.spec
https://medium.com/@badawekoo/kubectl-commands-you-need-to-know-part2-49de3602f255


### kubectl explain deployment.metadata
https://medium.com/@badawekoo/kubectl-commands-you-need-to-know-part2-49de3602f255


### kubectl explain svc.spec
https://medium.com/@badawekoo/kubectl-commands-you-need-to-know-part2-49de3602f255

### Dryrun

alias kdr='kubectl --dry-run=client -o yaml'

## kubectl get nodes

## kubectl get all -l app=nginx

## kubectl get all --show-labels
    NAME                             READY   STATUS    RESTARTS   AGE     LABELS
    pod/share-pod                    2/2     Running   0          26m     app=share-pod
    pod/webserver-7fb7fd49b4-ktrbl   1/1     Running   2          4d20h   app=nginx,pod-template-hash=7fb7fd49b4
    pod/webserver-7fb7fd49b4-qhz8m   1/1     Running   2          4d20h   app=nginx,pod-template-hash=7fb7fd49b4
    pod/webserver-7fb7fd49b4-xclrd   1/1     Running   2          4d20h   app=nginx,pod-template-hash=7fb7fd49b4

    NAME                  TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE     LABELS
    service/kubernetes    ClusterIP   10.96.0.1        <none>        443/TCP        4d21h   component=apiserver,provider=kubernetes
    service/web-service   NodePort    10.104.141.174   <none>        80:30961/TCP   4d20h   run=web-service

    NAME                        READY   UP-TO-DATE   AVAILABLE   AGE     LABELS
    deployment.apps/webserver   3/3     3            3           4d20h   app=nginx

    NAME                                   DESIRED   CURRENT   READY   AGE     LABELS
    replicaset.apps/webserver-7fb7fd49b4   3         3         3       4d20h   app=nginx,pod-template-hash=7fb7fd49b4

## kubectl get all -o wide
    NAME                             READY   STATUS    RESTARTS   AGE     IP           NODE       NOMINATED NODE   READINESS GATES
    pod/share-pod                    2/2     Running   0          26m     172.17.0.6   minikube   <none>           <none>
    pod/webserver-7fb7fd49b4-ktrbl   1/1     Running   2          4d20h   172.17.0.4   minikube   <none>           <none>
    pod/webserver-7fb7fd49b4-qhz8m   1/1     Running   2          4d20h   172.17.0.2   minikube   <none>           <none>
    pod/webserver-7fb7fd49b4-xclrd   1/1     Running   2          4d20h   172.17.0.3   minikube   <none>           <none>

    NAME                  TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE     SELECTOR
    service/kubernetes    ClusterIP   10.96.0.1        <none>        443/TCP        4d21h   <none>
    service/web-service   NodePort    10.104.141.174   <none>        80:30961/TCP   4d20h   app=nginx

    NAME                        READY   UP-TO-DATE   AVAILABLE   AGE     CONTAINERS   IMAGES         SELECTOR
    deployment.apps/webserver   3/3     3            3           4d20h   nginx        nginx:alpine   app=nginx

    NAME                                   DESIRED   CURRENT   READY   AGE     CONTAINERS   IMAGES         SELECTOR
    replicaset.apps/webserver-7fb7fd49b4   3         3         3       4d20h   nginx        nginx:alpine   app=nginx,pod-template-hash=7fb7fd49b4

## kubectl get po --all-namespaces
    NAMESPACE     NAME                               READY   STATUS    RESTARTS   AGE
    default       share-pod                          2/2     Running   0          4h52m
    default       webserver-7fb7fd49b4-ktrbl         1/1     Running   2          5d1h
    default       webserver-7fb7fd49b4-qhz8m         1/1     Running   2          5d1h
    default       webserver-7fb7fd49b4-xclrd         1/1     Running   2          5d1h
    kube-system   coredns-558bd4d5db-bzwvq           1/1     Running   2          5d1h
    kube-system   etcd-minikube                      1/1     Running   2          5d1h
    kube-system   kube-apiserver-minikube            1/1     Running   2          5d1h
    kube-system   kube-controller-manager-minikube   1/1     Running   3          5d1h
    kube-system   kube-proxy-hpkjg                   1/1     Running   2          5d1h
    kube-system   kube-scheduler-minikube            1/1     Running   2          5d1h
    kube-system   storage-provisioner                1/1     Running   5          5d1h

## kubectl get po
    NAME                         READY   STATUS    RESTARTS   AGE
    share-pod                    2/2     Running   0          4h53m
    webserver-7fb7fd49b4-ktrbl   1/1     Running   2          5d1h
    webserver-7fb7fd49b4-qhz8m   1/1     Running   2          5d1h
    webserver-7fb7fd49b4-xclrd   1/1     Running   2          5d1h

## kubectl get namespace
    NAME              STATUS   AGE
    default           Active   5d1h
    kube-node-lease   Active   5d1h
    kube-public       Active   5d1h
    kube-system       Active   5d1h

## kubectl get po share-pod -o json
    {
        "apiVersion": "v1",
        "kind": "Pod",
        "metadata": {
            "creationTimestamp": "2021-08-22T15:47:25Z",
            "labels": {
                "app": "share-pod"
            },
            "name": "share-pod",
            "namespace": "default",
            "resourceVersion": "5492",
            "uid": "7cb30d91-ddc4-449c-8c47-628f00aa68a7"
        },
        "spec": {
            "containers": [
                {...
        }
    }

## kubectl delete -f webserver-svc.yaml

## kubectl delete app=nginx

## kubectl create -f share-pod.yaml

## kubectl get pods -l app=share-pod -o jsonpath="{.items[*].spec.containers[*].image}" |tr -s '[[:space:]]' '\n' |sort |uniq -c
      1 debian
      1 nginx
Containers running inside a pod

## kubectl describe pod/share-pod
    Name:         share-pod
    Namespace:    default
    Priority:     0
    Node:         minikube/192.168.49.2
    Start Time:   Sun, 22 Aug 2021 22:55:51 +0200
    Labels:       app=share-pod
    Annotations:  <none>
    Status:       Running
    IP:           172.17.0.2
    IPs:
    IP:  172.17.0.2
    Containers:
    nginx:
        Container ID:   docker://b016bc18e6b95da3c018231b7cce2c0637bafbf3fdf671db7a20b9138e40c116
        Image:          nginx
        Image ID:       docker-pullable://nginx@sha256:4d4d96ac750af48c6a551d757c1cbfc071692309b491b70b2b8976e102dd3fef
        Port:           80/TCP
        Host Port:      0/TCP
        State:          Running
        Started:      Sun, 22 Aug 2021 22:56:04 +0200
        Ready:          True
        Restart Count:  0
        Environment:    <none>
        Mounts:
        /usr/share/nginx/html from host-volume (rw)
        /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-sddwz (ro)
    debian:
        Container ID:  docker://a68f9aafd4a82ed92cd7913240e79efb779f5992283cee87a0ddfcd6eacf91ad
        Image:         debian
        Image ID:      docker-pullable://debian@sha256:38988bd08d1a5534ae90bea146e199e2b7a8fca334e9a7afe5297a7c919e96ea
        Port:          <none>
        Host Port:     <none>
        Command:
        /bin/sh
        -c
        echo Introduction to Kubernetes > /host-vol/index.html; sleep 3600
        State:          Running
        Started:      Sun, 22 Aug 2021 22:56:11 +0200
        Ready:          True
        Restart Count:  0
        Environment:    <none>
        Mounts:
        /host-vol from host-volume (rw)
        /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-sddwz (ro)
    Conditions:
    Type              Status
    Initialized       True 
    Ready             True 
    ContainersReady   True 
    PodScheduled      True 
    Volumes:
    host-volume:
        Type:          HostPath (bare host directory volume)
        Path:          /home/villaraco/treball/docker/cursKubernetes/chapter12_volumes
        HostPathType:  
    kube-api-access-sddwz:
        Type:                    Projected (a volume that contains injected data from multiple sources)
        TokenExpirationSeconds:  3607
        ConfigMapName:           kube-root-ca.crt
        ConfigMapOptional:       <nil>
        DownwardAPI:             true
    QoS Class:                   BestEffort
    Node-Selectors:              <none>
    Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                                node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
    Events:
    Type    Reason     Age   From               Message
    ----    ------     ----  ----               -------
    Normal  Scheduled  11m   default-scheduler  Successfully assigned default/share-pod to minikube
    Normal  Pulling    11m   kubelet            Pulling image "nginx"
    Normal  Pulled     10m   kubelet            Successfully pulled image "nginx" in 2.066502138s
    Normal  Created    10m   kubelet            Created container nginx
    Normal  Started    10m   kubelet            Started container nginx
    Normal  Pulling    10m   kubelet            Pulling image "debian"
    Normal  Pulled     10m   kubelet            Successfully pulled image "debian" in 3.181057845s
    Normal  Created    10m   kubelet            Created container debian
    Normal  Started    10m   kubelet            Started container debian

## kubectl exec -it <pod> -c <container> -- bash

## kubectl run -it --rm --restart=Never busybox --image=gcr.io/google-containers/busybox sh
debugging for what a container sees in a pod

https://kubernetes.io/docs/tasks/debug-application-cluster/

## kubectl create configmap my-config --from-literal=key1=value1 --from-literal=key2=value2

## yaml for creation configmap
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: customer1
data:
  TEXT1: Customer1_Company
  TEXT2: Welcomes You
  COMPANY: Customer1 Company Technology Pct. Ltd.
```

## kubectl get configmaps my-config
```
NAME        DATA   AGE
my-config   2      85s
```

## kubectl get configmaps my-config -o json
```json
{
    "apiVersion": "v1",
    "data": {
        "key1": "value1",
        "key2": "value2"
    },
    "kind": "ConfigMap",
    "metadata": {
        "creationTimestamp": "2021-08-25T16:36:09Z",
        "name": "my-config",
        "namespace": "default",
        "resourceVersion": "10838",
        "uid": "a1565916-60b4-48d9-87c8-2ab8b9a64120"
    }
}
```

## Navigate to namespace
```
kubectl config set-context --current --namespace=app
```

## ingress pods in ingress-nginx
kubectl get pods -n ingress-nginx
```
NAME                                        READY   STATUS      RESTARTS      AGE
ingress-nginx-admission-create--1-jkm7f     0/1     Completed   0             13d
ingress-nginx-admission-patch--1-hxx2c      0/1     Completed   1             13d
ingress-nginx-controller-69bdbc4d57-82k89   1/1     Running     4 (75m ago)   13d
```

## List all pods versions (controllers versions)
https://kubernetes.io/docs/tasks/access-application-cluster/list-all-running-container-images/

kubectl get pods --all-namespaces -o jsonpath="{.items[*].spec.containers[*].image}" |\
tr -s '[[:space:]]' '\n' |\
sort |\
uniq -c
```yaml
      2 docker.io/calico/apiserver:v3.24.3
      1 docker.io/calico/kube-controllers:v3.24.3
      3 docker.io/calico/node:v3.24.3
      2 docker.io/calico/typha:v3.24.3
      1 quay.io/metallb/controller:v0.13.7
      3 quay.io/metallb/speaker:v0.13.7
      1 quay.io/tigera/operator:v1.28.3
      2 registry.k8s.io/coredns/coredns:v1.9.3
      1 registry.k8s.io/etcd:3.5.5-0
      2 registry.k8s.io/ingress-nginx/controller:v1.4.0@sha256:34ee929b111ffc7aa426ffd409af44da48e5a0eea1eb2207994d9e0c0882d143
      1 registry.k8s.io/kube-apiserver:v1.25.4
      1 registry.k8s.io/kube-controller-manager:v1.25.4
      3 registry.k8s.io/kube-proxy:v1.25.4
      1 registry.k8s.io/kube-scheduler:v1.25.4nstall - k8s - Obtenir la versió dels controladors instal·lats al Kubernetes
```


## minikube version
minikube version
```
minikube version: v1.23.0
commit: 5931455374810b1bbeb222a9713ae2c756daee10
```


## Kubernetes logs
kubectl logs <POD_NAME> -c <CONTAINER_NAME> -n <NAMESPACE>

$kubectl logs --since=1h <PodName>
$kubectl logs --tail=20 <PodName>


## Test if a service account can do certain action which will be in the below format and the result will be yes or no
https://medium.com/@badawekoo/kubectl-commands-you-need-to-know-d6a1bcd07bba

kubectl --as=system:serviceaccount:<NAMESPACE>:<SERVICE_ACCOUNT_NAME> auth can-i get configmap/<CONFIGMAP_NAME>


## Test if a user account can do certain action
https://medium.com/@badawekoo/kubectl-commands-you-need-to-know-d6a1bcd07bba

kubectl -n <NAMESPACE> auth can-i list secrets --as <USERACCOUNT>


## Kubernetes top
### Top pods in CPU and memory
kubectl top pod -n <NAMESPACE>


## Kubernetes cp
kubectl cp <NAMESPACE>/<PODNAME>:</path/to/file> <FILENAME>



## Kubernetes abbreviations
$kubectl get po -n <namespace>        ## po=pod ## -n=--namespace
$kubectl get svc                      ## svc=service
$kubectl get ns                       ## ns=namespace
$kubectl get deploy                   ## deploy=deployment
$kubectl get ds                       ## ds=daemonset
$kubectl get sa                       ## sa=serviceaccount
$kubectl get rs                       ## rs=replicaset
$kubectl get cm                       ## cm=configmap
$kubectl get ing                      ## ing=ingress
$kubectl get pv                       ## pv=persistentvolume
$kubectl get pvc                      ## pvc=persistentvolumeclaim
$kubectl get sc                       ## sc=storageclass


## Kubernetes drain
$ kubectl drain <NodeName>





# Libvirt
## List vm for all domains
virsh list --all   
                -> From root

virsh --connect qemu:///system list --all    
                                        -> From a regular user


export LIBVIRT_DEFAULT_URI='qemu:///system'
              -> In order to don't specify every time this qemu 

alias lVl="virsh list --all"
alias lVln="virsh net-dhcp-leases default"


## Start Libvirt network default
virsh net-start default


## Connect console
virsh console debian11-amd64


## List assigned IPs
virsh net-dhcp-leases default
 Expiry Time           MAC address         Protocol   IP address           Hostname        Client ID or DUID
-----------------------------------------------------------------------------------------------------------------------------------------------------
 2022-07-30 13:18:19   52:54:00:37:01:30   ipv4       192.168.122.234/24   -               01:52:54:00:37:01:30
 2022-07-30 13:54:15   52:54:00:37:01:30   ipv4       192.168.122.235/24   debian11-base   ff:00:37:01:30:00:01:00:01:2a:77:c3:df:52:54:00:37:01:30


virsh net-dumpxml default
```xml
<network connections='4'>
  <name>default</name>
  <uuid>a5763fdd-fde6-434f-ae8f-0b5c2ca21d27</uuid>
  <forward mode='nat'>
    <nat>
      <port start='1024' end='65535'/>
    </nat>
  </forward>
  <bridge name='virbr0' stp='on' delay='0'/>
  <mac address='52:54:00:b5:cb:14'/>
  <domain name='libvirt' localOnly='yes'/>
  <ip address='192.168.122.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.122.2' end='192.168.122.200'/>
      <host mac='52:54:00:00:2f:ca' name='kube_base' ip='192.168.122.2'/>
      <host mac='52:54:00:37:01:30' name='debian11-base' ip='192.168.122.3'/>
      <host mac='52:54:00:72:d0:c3' name='kube01' ip='192.168.122.4'/>
      <host mac='52:54:00:be:6d:ca' name='kube03' ip='192.168.122.6'/>
      <host mac='52:54:00:1f:67:19' name='kube02' ip='192.168.122.5'/>
      <host mac='52:54:00:ad:84:40' name='kube04' ip='192.168.122.7'/>
    </dhcp>
  </ip>
</network>
```


## Start all VMs
for i in {1..4}; do virsh start kube0${i}-d11; done

## libvirt restart
systemctl start libvirtd

## libvirt logs
journalctl -u libvirt [-f]

LIBVIRT_DEBUG=1 virt-manager --no-fork

tail -f /var/log/syslog /var/log/libvirt/qemu/diba-win10.log

tail -f /home/villaraco/.cache/virt-manager/virt-manager.log




# nmap
## Fast scan: nmap -sn 192.168.1.0/24


# ssh
## ssh execute

```bash
# ssh
## ssh execute

#!/bin/bash
# -*- ENCODING: UTF-8 -*-

servers=('pre01-su0472' 'pre02-su0476' 'pre03-su0484' 'proWeb01-su0482' \
         'proWeb02-su0475' 'proWeb03-su0493' 'proDB1-su0473' 'proDB2-su0474' \
         'root@su0708' 'root@su0289' \
         'adm-su0530')

for elem in ${!servers[@]}; do
    echo "$elem is ${servers[$elem]}" 
    ssh ${servers[$elem]} "yum -q list installed logwatch.noarch" 
done
```

# Flask
## Blueprint dummy_test
```python
# This funtion has been intencionally left commented in order easy test
#   blueprint architecture. Please refer to tests/test_project_publicproject.py
#   in funtion test_dummy_test() (also comented)
@publicprojects_views.route("/dummy_test", methods=["GET"])
@publicprojects_views.route("/dummy_test/", methods=["GET"])
@publicprojects_views.route("/dummy_test/<string:dummy_str>", methods=["GET"])
def dummy_test(dummy_str="pepe"):
    """Dummy test without auth in order to easy test blueprint"""
    return jsonify(success=True, param=dummy_str)
```

## Blueprint default route
```python
@publicprojects_views.route("/", defaults={"project_order": "created-desc"},
                            methods=["GET"])
@publicprojects_views.route("/<string:project_order>", methods=["GET"])
@provider.require_oauth()
@quotes_token()
@keysecurity([POLICIES.ProjectPolicies.KINNOVATION_READ_PUBLICPROJECT])
@swag_from("publicprojectindex.yml")
def get(project_order, auth=None):
    return jsonify(success=True)
```

## debug mode run
https://flask.palletsprojects.com/en/1.1.x/quickstart/#debug-mode
```bash
export FLASK_APP=aaaservice
export FLASK_ENV=development
export FLASK_DEBUG=0
```
Development mode: automaticaly restarts Flask and active debug mode.

FLASK_DEBUG=0: activa el modo debug sin la rearrancada de flask ante cambios.

## debug and development mode
FLASK_DEBUG=1 FLASK_ENV=development flask run

## Debugging with vscode (No docker)
https://code.visualstudio.com/docs/python/tutorial-flask







 





# Suport als projectes
## Matriu RACI
https://es.wikipedia.org/wiki/Matriz_de_asignaci%C3%B3n_de_responsabilidades

* Responsible - Responsable
* Accountable - Aprobador
* Consulted - Consultado
* Informed - Informat



